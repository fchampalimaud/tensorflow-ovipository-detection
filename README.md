# TENSORFLOW DEEP NEURAL NETWORK FRAMEWORK

The purpose of this framework is to allow any user to create and train neural networks using the Tensorflow library runnig on the Champalimaud Foundation cluster.

The development of this framework follows the development of an ovipositor extrusion detector, which will serve as the example for explaining how the framework works.

## General Concept

The overall design of this framework aims for any user to build and train neural networks with little effort and almost no coding skills. This is accomplished by allowing the user to always run the same job in the cluster, feeding a configuration file with the definitions for the input section of the neural network, and another configuration file with the actual neural network design. 

The result of this process is a snapshot file of the neural network, traied for detection of the events detailed in the input configuration file. For the ovipositor extrusion project, the network is used to analyze a video sequence and extract, for each video frame, the probability of ovipositor extrusion for each fly in the scene. The collected is compiled as a Python Video Anotator project for easy result check.

## Configuration Files

This section aims for explaining the configuration files. 

### Neural Network Design Configuration

In order to train a neural network, the user must first define it's design.
For image classification problems, and consequently the ovipositor extrusion project, neural networks normaly use convolutional layers and fully connected layers. In order to define the network design, a JSON style configuration file must be created. Below is an example of a simple neural network design for basic explanation of the configuration structure

``` json
{
    "cnn1layer_8_mp": {
        "conv1": {
            "type": "CONV2D",
            "num_input_channels": 3,
            "conv_filter_size": 3,
            "num_filters": 8,
            "use_maxpool": true
        },
        "flatten": {
            "type": "FLATTEN"
        },
        "fc1": {
            "type": "FULLYCONNECTED",
            "num_outputs": 8,
            "use_relu": true
        },
        "fc2": {
            "type": "FULLYCONNECTED",
            "num_outputs": 2,
            "use_relu": false
        }
    }
}
```

This example reprensents a JSON file containing one neural network configuration. When creating this file, the neural network design must contain a tag that identifies it. In this case, the tag is "cnn1layer_8_mp". Inside this tag there are several different tags, each one having it's own name (chosen by the user) and representing a different network layer. Each layer has a type and specific atributes that must be defined according to it's type.

This network is composed by a single convolutional layer (CONV2D) followed by a flatten (FLATTEN) layer to prepare the data for the next layers, which are called dense layers (FULLYCONNECTED). The meaning of the different names and attributes is detailed below:

The "type" tag defines the type of layer:
        
- CONV2D: Convolutional layer
- FLATTEN: Flatten layer (does not have any settings)
- FULLYCONNECTED: Dense layer

For the convolutional layer, these settings must be defined:

- num_input_channels: The number of channels of the image. 1 for grayscale, 3 for color images
- conv_filter_size: The kernel size for each convolutional filter. Odd numbers only
- num_filters: The number of filters of the convolutional layer
- use_maxpool: Add a max pooling unit at the end of the layer. "true" or "false" values accepted

For the dense layer, these settings must be defined:

- num_outputs: The number of outputs. If this is the last layer of the network, the number of outputs must be equal to the number of classes defined
- use_relu: Add a rectified linear unit (Relu) at the end of the layer. "true" or "false" values accepted

Having defined these settings, the algorithm running on the cluster will build the neural network according to the established design.

The design structure described above can easly be changed by the user in order to create different networks with different designs. Also, the same file can have multiple network designs. The next example shows a different file idea containing two different networks, named "cnn1layer_8_mp" and "cnn3layer_64_mp".

``` json
{
    "cnn1layer_8_mp": {
        "conv1": {
            "type": "CONV2D",
            "num_input_channels": 3,
            "conv_filter_size": 3,
            "num_filters": 8,
            "use_maxpool": true
        },
        "flatten": {
            "type": "FLATTEN"
        },
        "fc1": {
            "type": "FULLYCONNECTED",
            "num_outputs": 8,
            "use_relu": true
        },
        "fc2": {
            "type": "FULLYCONNECTED",
            "num_outputs": 2,
            "use_relu": false
        }
    },
    "cnn3layer_64_mp": {
        "conv1": {
            "type": "CONV2D",
            "num_input_channels": 3,
            "conv_filter_size": 3,
            "num_filters": 64,
            "use_maxpool": true
        },
        "conv2": {
            "type": "CONV2D",
            "num_input_channels": 64,
            "conv_filter_size": 3,
            "num_filters": 64,
            "use_maxpool": true
        },
        "conv3": {
            "type": "CONV2D",
            "num_input_channels": 64,
            "conv_filter_size": 3,
            "num_filters": 64,
            "use_maxpool": true
        },
        "flatten": {
            "type": "FLATTEN"
        },
        "fc1": {
            "type": "FULLYCONNECTED",
            "num_outputs": 64,
            "use_relu": true
        },
        "fc2": {
            "type": "FULLYCONNECTED",
            "num_outputs": 2,
            "use_relu": false
        }
    }
}
```
Since the purpose of cluster processing is to run several jobs in parallel, this type of file arrangement allows the user to easly train different neural networks using the same input files, or train the same neural network using different input files. Detailed description of the input files is shown below.

### Input File Configuration

This section describes how the input configuration file is structured. Below we have an example input configuration file for training a neural network for the ovipositor extrusion project.

```json
{
    "input": {
        "classes": [
            "extrusion",
            "noextrusion"
        ],
        "image_size": 50,
        "samples_per_folder": 2500,
        "epochs": 100,
        "data": {
            "folder1": {
                "path": "extrusion3_tail/",
                "label": 0
            },
            "folder2": {
                "path": "female_tail/",
                "label": 1
            }
        }
    }
}
```
Following this structure, these are the settings definition:

- classes: An array with the names of each class.
- image_size: the size of the images to be fed into the training phase of the neural network. If the training is fed with 
- samples_per_folder: The number of images to be read by the neural network for training
- epochs: the number of times the trainig process goes through all the training images

The data structure contains the list of the folders where the algorithm is going to get the training images. Each item in this list contains the following information:

- path: The path to the specified folder (cluster location. More on this later)
- label: The class to which the set of images belongs

## Neural Network Training

This section describes the steps for training a neural network

### File Preparation

For neural network training, the first thing the user needs to upload to the cluster is the code folder, containing all the code needed to create and train the neural networks. The next thing the user needs is to upload two folders containing the training images for both extrusion / no extrusion cases.

### Condor Script

```
universe = vanilla
executable = /usr/bin/python3

InitialDir= /cluster/vasconcelos/vasconcelos2/tensorflow-dnn

getenv = true
stream_output = true
request_cpus = 1
request_memory = 1024
request_gpus = 1
environment = "LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64"

transfer_input_files = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/Code, /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/Datasets/extrusion2_tail,/cluster/vasconcelos/vasconcelos2/tensorflow-dnn/Datasets/female_tail

arguments = Code/neural_net_trainer.py Code/input_3.json Code/extrusion_networks_3layer.json cnn3layer_64_mp cnn3layer_64_mp_in3
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in3.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in3.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in3.log

queue
```

Since this is a python job, the first two variable in the configuration file that you MUST NOT change which are the universe (is always vanilla) and executable (python of course).

(Don't actually know if we need the InitialDir)

After that we have some variables concering the cluster configuration for the type of job we want to run. This is due to the fact that neural network jobs are done using the cluster's GPUs.

The we have to transfer all the files needed for the job to run correctly. In this case, we need to transfer the folder containing the code, and the folders corresponding to the datasets.

The arguments of your job are passed through using the arguments keyword and the first argument MUST be the python module (.py file) that you want to run followed by an additional parameters 
of your script. These are sent as if their were typed in the terminal. For training purposes, the arguments are:

- The main function to be called, that is inside the Code folder: Code/neural_net_trainer.py
- The input configuration file, that is inside the Code folder: Code/input_3.json 
- The neural network design configuration file, that is inside de Code folder: Code/extrusion_networks_3layer.json 
- The neural network tag corresponding to the design to be built: cnn3layer_64_mp
- The output filename for the trained neural network weights: cnn3layer_64_mp_in3

The variables output, error and log are used to keep track of information related to the output of the job.

The "queue" line indicates that a job is configured and it's instance is ready to be crated.

### Running
The last step is to use the command line to call the script file we created above, nagivating to the folder where the script is and calling the forrlowing command

```
condor_submit example_job.sh
```

## Using a trained neural network - Ovipositor Extrusion Example

### File Preparation

### Condor Script

```
# CONFIGURATION VARIABLES ###############################################
PYTHON_CODE = /cluster/vasconcelos/vasconcelos2/cecelia.mezzera/tensorflow-OE/Code
TF_NETS     = /cluster/vasconcelos/vasconcelos2/cecelia.mezzera/tensorflow-OE/Nets
VIDEO_DIRS  = /cluster/vasconcelos/vasconcelos2/cecelia.mezzera/movies
#########################################################################

INITIALDIR 			  = $(VIDEO_DIRS)/$(VIDEO)
OUTPUT 				  = condor-output.out
ERROR 				  = condor-error.err
LOG 				  = condor-log.log
TRANSFER_OUTPUT_FILES             = videoannotator-project
TRANSFER_INPUT_FILES              = $(PYTHON_CODE), $(TF_NETS), $(VIDEO_DIRS)/$(VIDEO)

getenv 		= true
stream_output 	= true
request_cpus 	= 1
request_memory 	= 1024
request_gpus 	= 1
environment     = "LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64"
universe        = vanilla
executable      = /usr/bin/python3


ARGUMENTS = Code/process_video.py Code/input_4.json Code/extrusion_networks_3layer.json cnn3layer_64_mp Nets/cnn3layer_64_mp_in4  $(VIDEO)/$(VIDEO).avi

VIDEO = video71_2017-05-29T11_40_03
queue

VIDEO = video93_2017-05-31T10_54_51
queue

VIDEO = video107_2017-06-01T10_54_20
queue


VIDEO = video83_2017-05-30T11_32_45
queue


VIDEO = video24_2017-05-18T11_16_27
queue
```
### Running

This step flollows the same rule as if we were training a neural network:

```
condor_submit process_video.sh
```

## Extrusion probability computation: distance refinement

Later through the project, a small update was added to the extrusion probability computations, now taking into accound the actual distance between two flies. This step was created independently from the neural network processes so it could run in more machines for faster processing of multiple videos.

For this step, here is an example of a condor script

```
# CONFIGURATION VARIABLES ###############################################
PYTHON_CODE = /cluster/vasconcelos/vasconcelos2/cecelia.mezzera/tensorflow-OE/Code
TF_NETS     = /cluster/vasconcelos/vasconcelos2/cecelia.mezzera/tensorflow-OE/Nets
VIDEO_DIRS  = /cluster/vasconcelos/vasconcelos2/cecelia.mezzera/4_1549_Chrimson_competition/OutputCompetition
#########################################################################

INITIALDIR 			  = $(VIDEO_DIRS)/$(VIDEO)
OUTPUT 				  = condor-output.out
ERROR 				  = condor-error.err
LOG 				  = condor-log.log
TRANSFER_OUTPUT_FILES = $(VIDEO)/videoannotator-extended
TRANSFER_INPUT_FILES  = $(PYTHON_CODE), $(TF_NETS), $(VIDEO_DIRS)/$(VIDEO)

getenv 			= true
stream_output 	= true
request_cpus 	= 1
request_memory 	= 1024
universe 		= vanilla
executable 		= /usr/bin/python3

ARGUMENTS = Code/DISTPROB.py $(VIDEO)

VIDEO = video13_2017-11-10T00_32_29
queue


VIDEO = video19_2017-11-17T10_41_17
queue


VIDEO = video36_2017-11-21T10_37_14
queue


VIDEO = video5_2017-11-09T23_01_34
queue
```

