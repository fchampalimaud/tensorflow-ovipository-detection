.. TensorFlow-DNN documentation master file, created by
   sphinx-quickstart on Wed Dec  6 10:51:49 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TensorFlow-DNN's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   tutorial

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
