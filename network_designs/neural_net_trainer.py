import sys
import tensorflow_networks.native.tensorflow_net_builder as networks
import tensorflow_networks.tools as tools

import tensorflow_networks.dataset as dataset

import os


def main(argv):

    input_file = argv[0]
    network_file = argv[1]
    network_name = argv[2]
    out_network = argv[3]
    
    input_file_noex = os.path.splitext(input_file)[0]

    #D:\CODE\Videos\video36_2017-05-26T10_19_49\Images\contours-images\video36_2017-05-26T10_19_49
    print('starting neural network')

    design, image_size, imgclasses, num_samples, data, epochs = tools.netparser_2(
        input_file, network_file,network_name)

    model = networks.netbuilder()
    model.GetImages(data, image_size, 3, imgclasses, 0.2, num_samples)
    print('images read')
    model.GenerateModel(design)
    model.Train(n_epochs=epochs, _batch_size=16, output_interval=20)
    model.Save(out_network + ".tf")


if __name__ == '__main__':
    '''
    This shall receive as arguments the input json, the network json and the network name
    '''
    main(sys.argv[1:])