import os
import glob
from sklearn.utils import shuffle
from skimage import color, io
from scipy.misc import imresize
import numpy as np
import cv2


def rotate_image(image, angle):
    """
	  Rotates the given image about it's centre
	  """

    image_size = (image.shape[1], image.shape[0])
    image_center = tuple(np.array(image_size) / 2)

    rot_mat = np.vstack(
        [cv2.getRotationMatrix2D(image_center, angle, 1.0), [0, 0, 1]])
    trans_mat = np.identity(3)

    w2 = image_size[0] * 0.5
    h2 = image_size[1] * 0.5

    rot_mat_notranslate = np.matrix(rot_mat[0:2, 0:2])

    tl = (np.array([-w2, h2]) * rot_mat_notranslate).A[0]
    tr = (np.array([w2, h2]) * rot_mat_notranslate).A[0]
    bl = (np.array([-w2, -h2]) * rot_mat_notranslate).A[0]
    br = (np.array([w2, -h2]) * rot_mat_notranslate).A[0]

    x_coords = [pt[0] for pt in [tl, tr, bl, br]]
    x_pos = [x for x in x_coords if x > 0]
    x_neg = [x for x in x_coords if x < 0]

    y_coords = [pt[1] for pt in [tl, tr, bl, br]]
    y_pos = [y for y in y_coords if y > 0]
    y_neg = [y for y in y_coords if y < 0]

    right_bound = np.max(x_pos)
    left_bound = np.min(x_neg)
    top_bound = np.max(y_pos)
    bot_bound = np.min(y_neg)

    new_w = int(np.abs(right_bound - left_bound))
    new_h = int(np.abs(top_bound - bot_bound))
    new_image_size = (new_w, new_h)

    new_midx = new_w * 0.5
    new_midy = new_h * 0.5

    dx = int(new_midx - w2)
    dy = int(new_midy - h2)

    trans_mat = getTranslationMatrix2d(dx, dy)
    affine_mat = (np.matrix(trans_mat) * np.matrix(rot_mat))[0:2, :]
    result = cv2.warpAffine(
        image, affine_mat, new_image_size, flags=cv2.INTER_LINEAR)

    return result


def load_train(train_path, image_size, classes, num_samples=0):
    images = []
    labels = []
    img_names = []
    cls = []

    print('Going to read training images')
    for fields in classes:
        index = classes.index(fields)
        print('Now going to read {} files (Index: {})'.format(fields, index))
        path = os.path.join(train_path, fields, '*g')
        files = glob.glob(path)
        count = 0
        for fl in files:
            img = io.imread(folder + f)
            new_img = imresize(img, (image_size, image_size, 3))
            image = new_img.astype(np.float32)
            image = np.multiply(image, 1.0 / 255.0)
            images.append(image)
            label = np.zeros(len(classes))
            label[index] = 1.0
            labels.append(label)
            image = rotate_image(image, 25)
            images.append(image)
            label = np.zeros(len(classes))
            label[index] = 1.0
            labels.append(label)
            image = rotate_image(image, 25)
            images.append(image)
            label = np.zeros(len(classes))
            label[index] = 1.0
            labels.append(label)
            image = rotate_image(image, 25)
            images.append(image)
            label = np.zeros(len(classes))
            label[index] = 1.0
            labels.append(label)
            image = rotate_image(image, 25)
            images.append(image)
            label = np.zeros(len(classes))
            label[index] = 1.0
            labels.append(label)
            flbase = os.path.basename(fl)
            img_names.append(flbase)
            cls.append(fields)
            count = count + 1
            if num_samples > 0 and count == num_samples:
                break
    images = np.array(images)
    labels = np.array(labels)
    img_names = np.array(img_names)
    cls = np.array(cls)

    return images, labels, img_names, cls


class DataSet(object):
    def __init__(self, images, labels, img_names, cls):
        self._num_examples = images.shape[0]

        self._images = images
        self._labels = labels
        self._img_names = img_names
        self._cls = cls
        self._epochs_done = 0
        self._index_in_epoch = 0

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def img_names(self):
        return self._img_names

    @property
    def cls(self):
        return self._cls

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_done(self):
        return self._epochs_done

    def next_batch(self, batch_size):
        """Return the next `batch_size` examples from this data set."""
        start = self._index_in_epoch
        self._index_in_epoch += batch_size

        if self._index_in_epoch > self._num_examples:
            # After each epoch we update this
            self._epochs_done += 1
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch

        return self._images[start:end], self._labels[
            start:end], self._img_names[start:end], self._cls[start:end]


def read_train_sets(train_path,
                    image_size,
                    classes,
                    validation_size,
                    num_samples=0):
    class DataSets(object):
        pass

    data_sets = DataSets()

    images, labels, img_names, cls = load_train(train_path, image_size,
                                                classes, num_samples)
    images, labels, img_names, cls = shuffle(images, labels, img_names, cls)

    if isinstance(validation_size, float):
        validation_size = int(validation_size * images.shape[0])

    validation_images = images[:validation_size]
    validation_labels = labels[:validation_size]
    validation_img_names = img_names[:validation_size]
    validation_cls = cls[:validation_size]

    train_images = images[validation_size:]
    train_labels = labels[validation_size:]
    train_img_names = img_names[validation_size:]
    train_cls = cls[validation_size:]

    data_sets.train = DataSet(train_images, train_labels, train_img_names,
                              train_cls)
    data_sets.valid = DataSet(validation_images, validation_labels,
                              validation_img_names, validation_cls)

    return data_sets


def load_train_folder(folder,
                      target_class,
                      classes,
                      image_size,
                      num_samples_per_folder=0):
    images = []
    labels = []
    img_names = []
    cls = []

    count = 0

    for f in os.listdir(folder):
        try:
            print('reading image', f)
            img = None
            img = cv2.imread(folder+f)
            if img is not None:
                #img = io.imread(folder + f)
                new_img = imresize(img, (image_size, image_size, 3))
                image = new_img.astype(np.float32)
                image = np.multiply(image, 1.0 / 255.0)
                images.append(image)
                label = np.zeros(len(classes))
                label[target_class] = 1.0
                labels.append(label)
                flbase = os.path.basename(f)
                img_names.append(flbase)
                cls.append(classes[target_class])
                count = count + 1
                if num_samples_per_folder > 0 and count == num_samples_per_folder:
                    break
        except ValueError:
            print('image not found')
    images = np.array(images)
    labels = np.array(labels)
    img_names = np.array(img_names)
    cls = np.array(cls)

    return images, labels, img_names, cls


def read_train_folders(folders,
                       classes,
                       validation_size,
                       image_size,
                       num_samples_per_folder=0):
    class DataSets(object):
        pass

    data_sets = DataSets()

    images = []
    labels = []
    img_names = []
    cls = []

    for f in folders:
        if len(images) == 0:
            images, labels, img_names, cls = load_train_folder(
                f[0], f[1], classes, image_size, num_samples_per_folder)
        else:
            _images, _labels, _img_names, _cls = load_train_folder(
                f[0], f[1], classes, image_size, num_samples_per_folder)
            images = np.concatenate((images, _images), axis=0)
            labels = np.concatenate((labels, _labels), axis=0)
            img_names = np.concatenate((img_names, _img_names), axis=0)
            cls = np.concatenate((cls, _cls), axis=0)

    images, labels, img_names, cls = shuffle(images, labels, img_names, cls)

    if isinstance(validation_size, float):
        validation_size = int(validation_size * images.shape[0])

    validation_images = images[:validation_size]
    validation_labels = labels[:validation_size]
    validation_img_names = img_names[:validation_size]
    validation_cls = cls[:validation_size]

    train_images = images[validation_size:]
    train_labels = labels[validation_size:]
    train_img_names = img_names[validation_size:]
    train_cls = cls[validation_size:]

    data_sets.train = DataSet(train_images, train_labels, train_img_names,
                              train_cls)
    data_sets.valid = DataSet(validation_images, validation_labels,
                              validation_img_names, validation_cls)

    return data_sets
