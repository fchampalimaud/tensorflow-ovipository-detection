'''
This Molule tries to make neural network building as simple 
as passing an array with its basic design
'''
from tensorflow.contrib import keras
import numpy as np
import time
import math
import random
from numpy.random import seed
from scipy.misc import imresize
import tensorflow_networks.dataset
from skimage import color, io

seed(1)
from tensorflow import set_random_seed
set_random_seed(2)

INPUT = 0
CONV2D = 1
FLATTEN = 2
FULLYCONNECTED = 3

# The next functions help creating the network layers
def create_weights(shape):
    pass


def create_biases(size):
    pass


def create_convolutional_layer(conv_filter_size,
                               num_filters,
                               use_maxpool=True):
    pass

def create_max_pool(input, size=2, stride=2):
    pass


def create_relu(input):
    pass


def create_flatten_layer(layer):
    pass


def create_fc_layer(input, num_inputs, num_outputs, use_relu=True):
    pass


class netbuilder:
    def GenerateModel(self, design, predicting=False):
        self.net = keras.models.Sequential()
        for i in range(0,len(design)):
            d = design[i]
            if d[0] == CONV2D:
                if i == 0:
                    self.net.add(keras.layers.Input(shape=[self.image_size, self.image_size, self.num_chanels]))
                self.net.add(create_convolutional_layer(conv_filter_size =d[3],num_filters=d[4])
            elif d[0] == FLATTEN:
                pass
            elif d[0] == FULLYCONNECTED:

        #finalize all the stuff... optimizers and shit

    def GenerateInput(self, _image_size=160, _num_chanels=3, _classes=['',
                                                                       '']):
        self.image_size = _image_size
        self.num_chanels = _num_chanels

    def GetImages(self,
                  folder='',
                  _image_size=160,
                  _num_chanels=3,
                  _classes=['', ''],
                  _validation_size=0.2,
                  num_samples=0):
        self.image_size = _image_size
        self.num_chanels = _num_chanels

    def Load(self, filename):
        pass

    def Save(self, filename):
        pass

    def show_progress(self, epoch, step, feed_dict_train, feed_dict_validate,
                      val_loss):
        pass

    def Train(self, n_epochs=10, _batch_size=16, output_interval=50):
        pass

    def predict(self, img):
        pass

    def __init__(self):
        pass