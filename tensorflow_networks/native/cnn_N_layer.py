import tensorflow as tf
import numpy as np
import time
import math
import random
from numpy.random import seed
from tensorflow_networks import dataset

seed(1)
from tensorflow import set_random_seed
set_random_seed(2)

# The next functions help creating the network layers

def create_weights(shape):
    return tf.Variable(tf.truncated_normal(shape, stddev=0.05))


def create_biases(size):
    return tf.Variable(tf.constant(0.05, shape=[size]))


def create_convolutional_layer(input,
                               num_input_channels, 
                               conv_filter_size,        
                               num_filters):  
    
    ## We shall define the weights that will be trained using create_weights function.
    weights = create_weights(shape=[conv_filter_size, conv_filter_size, num_input_channels, num_filters])
    ## We create biases using the create_biases function. These are also trained.
    biases = create_biases(num_filters)

    ## Creating the convolutional layer
    layer = tf.nn.conv2d(input=input,
                         filter=weights,
                         strides=[1, 1, 1, 1],
                         padding='SAME')

    layer += biases

    ## We shall be using max-pooling.  
    layer = tf.nn.max_pool(value=layer,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 2, 2, 1],
                           padding='SAME')
    ## Output of pooling is fed to Relu which is the activation function for us.
    layer = tf.nn.relu(layer)

    return layer

    

def create_flatten_layer(layer):
    #We know that the shape of the layer will be [batch_size img_size img_size num_channels] 
    # But let's get it from the previous layer.
    layer_shape = layer.get_shape()

    ## Number of features will be img_height * img_width* num_channels. But we shall calculate it in place of hard-coding it.
    num_features = layer_shape[1:4].num_elements()

    ## Now, we Flatten the layer so we shall have to reshape to num_features
    layer = tf.reshape(layer, [-1, num_features])

    return layer


def create_fc_layer(input,          
                    num_inputs,    
                    num_outputs,
                    use_relu=True):
    
    #Let's define trainable weights and biases.
    weights = create_weights(shape=[num_inputs, num_outputs])
    biases = create_biases(num_outputs)

    # Fully connected layer takes input x and produces wx+b.Since, these are matrices, we use matmul function in Tensorflow
    layer = tf.matmul(input, weights) + biases
    if use_relu:
        layer = tf.nn.relu(layer)

    return layer

class cnn_N_layer:
    
    def GenerateModel(self):
        ##The model
        self.layer_conv1 = create_convolutional_layer(input=self.x,
                                                      num_input_channels=self.num_channels,
                                                      conv_filter_size=self.filter_size_conv1,
                                                      num_filters=self.num_filters_conv1)
        self.layer_conv1 = create_convolutional_layer(input=self.layer_conv1,
                                                      num_input_channels=self.num_filters_conv1,
                                                      conv_filter_size=self.filter_size_conv2,
                                                      num_filters=self.num_filters_conv2)
        self.layer_conv1= create_convolutional_layer(input=self.layer_conv1,
                                                     num_input_channels=self.num_filters_conv2,
                                                     conv_filter_size=self.filter_size_conv3,
                                                     num_filters=self.num_filters_conv3)
        self.layer_conv1 = create_flatten_layer(self.layer_conv1)
        self.layer_conv1 = create_fc_layer(input=self.layer_conv1,
                                         num_inputs=self.layer_conv1.get_shape()[1:4].num_elements(),
                                         num_outputs=self.fc_layer_size,
                                         use_relu=True)

        self.layer_conv1 = create_fc_layer(input=self.layer_conv1,
                                         num_inputs=self.layer_conv1.get_shape()[1:4].num_elements(),
                                         num_outputs=self.num_classes,
                                         use_relu=False) 
        # you can get the probability of each class by applying softmax to the output of fully connected layer.
        self.y_pred = tf.nn.softmax(self.layer_conv1,name='y_pred')
        # y_pred contains the predicted probability of each class for each input image. 
        # The class having higher probability is the prediction of the network.
        self.y_pred_cls = tf.argmax(self.y_pred, dimension=1)
        # Now, let’s define the cost that will be minimized to reach the optimum value of weights.
        # We will use a simple cost that will be calculated using a Tensorflow function softmax_cross_entropy_with_logits 
        # which takes the output of last fully connected layer and actual labels to calculate cross_entropy whose average will give us the cost.
        self.session.run(tf.global_variables_initializer())
        self.cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=self.layer_conv1,
                                                                     labels=self.y_true)
        self.cost = tf.reduce_mean(self.cross_entropy)
        # Tensorflow implements most of the optimisation functions. We shall use AdamOptimizer for gradient calculation and weight optimization.
        # We shall specify that we are trying to minimise cost with a learning rate of 0.0001.
        self.optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(self.cost)
        # We can calculate the accuracy on validataion set using true labels(y_true) and predicted labels(y_pred).
        self.correct_prediction = tf.equal(self.y_pred_cls, self.y_true_cls)
        self.accuracy = tf.reduce_mean(tf.cast(self.correct_prediction, tf.float32))
        self.session.run(tf.global_variables_initializer())
        self.total_iterations = 0


    def GetImages(self, folder = '', _image_size=160, _num_chanels=3, _classes = ['',''], _validation_size = 0.2,num_samples = 0):
        self.classes = _classes
        self.num_classes = len(self.classes)
        # 20% of the data will automatically be used for validation
        self.validation_size = _validation_size
        self.img_size = _image_size
        self.num_channels = _num_chanels
        self.session = tf.Session()
        self.x = tf.placeholder(tf.float32, shape=[None, self.img_size,self.img_size,self.num_channels], name='x')
        self.y_true = tf.placeholder(tf.float32, shape=[None, self.num_classes], name='y_true')
        self.y_true_cls = tf.argmax(self.y_true, dimension=1)
        self.data = dataset.read_train_folders(folder, self.classes, self.validation_size, self.img_size, num_samples)

    def Load(self, filename):
        print('load')


    def Save(self, filename):
        self.saver.save(self.session,'./'+filename)
        print('save')

    def show_progress(self,epoch, step, feed_dict_train, feed_dict_validate, val_loss):
        acc = self.session.run(self.accuracy, feed_dict=feed_dict_train)
        val_acc = self.session.run(self.accuracy, feed_dict=feed_dict_validate)
        msg = "Training Epoch {0} Step {1} --- Training Accuracy: {2:>6.1%}, Validation Accuracy: {3:>6.1%},  Validation Loss: {4:.3f}"
        print(msg.format(epoch + 1, step, acc, val_acc, val_loss))


    def Train(self,n_epochs = 10, _batch_size = 16, info_interval = 10):
        self.batch_size = _batch_size
        self.saver = tf.train.Saver()
        print('train')
        step = 0
        epoch = 0
        while epoch < n_epochs:

            x_batch, y_true_batch, _, cls_batch = self.data.train.next_batch(self.batch_size)
            x_valid_batch, y_valid_batch, _, valid_cls_batch = self.data.valid.next_batch(self.batch_size)

            feed_dict_tr = {self.x: x_batch,
                            self.y_true: y_true_batch}
            feed_dict_val = {self.x: x_valid_batch,
                             self.y_true: y_valid_batch}

            self.session.run(self.optimizer, feed_dict=feed_dict_tr)

            #if i % int(self.data.train.num_examples/self.batch_size) == 0: 
            val_loss = self.session.run(self.cost, feed_dict=feed_dict_val)
            epoch = int(step / int(self.data.train.num_examples/self.batch_size))
            if step % info_interval == 0:    
                self.show_progress(epoch, step, feed_dict_tr, feed_dict_val, val_loss)
            step = step + 1       
        self.total_iterations += 1

    def __init__(self):
        print('init')
        ##Network graph params
        self.filter_size_conv1 = 3 
        self.num_filters_conv1 = 32
        self.filter_size_conv2 = 3
        self.num_filters_conv2 = 32
        self.filter_size_conv3 = 3
        self.num_filters_conv3 = 64
        self.fc_layer_size = 128
        