'''
This Molule tries to make neural network building as simple 
as passing an array with its basic design
'''
import tensorflow as tf
import numpy as np
import time
import math
import random
from numpy.random import seed
from scipy.misc import imresize
import tensorflow_networks.dataset
from skimage import color, io

seed(1)
from tensorflow import set_random_seed
set_random_seed(2)

INPUT = 0
CONV2D = 1
FLATTEN = 2
FULLYCONNECTED = 3


# The next functions help creating the network layers
def create_weights(shape):
    return tf.Variable(tf.truncated_normal(shape, stddev=0.05))


def create_biases(size):
    return tf.Variable(tf.constant(0.05, shape=[size]))


def create_convolutional_layer(input,
                               num_input_channels,
                               conv_filter_size,
                               num_filters,
                               use_maxpool=True):

    ## We shall define the weights that will be trained using create_weights function.
    weights = create_weights(shape=[
        conv_filter_size, conv_filter_size, num_input_channels, num_filters
    ])
    ## We create biases using the create_biases function. These are also trained.
    biases = create_biases(num_filters)

    ## Creating the convolutional layer
    layer = tf.nn.conv2d(
        input=input, filter=weights, strides=[1, 1, 1, 1], padding='SAME')

    layer += biases
    '''
    ## We shall be using max-pooling.  
    layer = tf.nn.max_pool(value=layer,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 2, 2, 1],
                           padding='SAME')
    ## Output of pooling is fed to Relu which is the activation function for us.
    layer = tf.nn.relu(layer)
    '''
    if use_maxpool is True:
        layer = create_max_pool(layer, 2, 2)

    layer = create_relu(layer)

    return layer


def create_max_pool(input, size=2, stride=2):
    ## We shall be using max-pooling.
    print('ADDING MAX POOL')
    layer = tf.nn.max_pool(
        value=input,
        ksize=[1, size, size, 1],
        strides=[1, stride, stride, 1],
        padding='SAME')
    return layer


def create_relu(input):
    print('ADDING RELU')
    layer = tf.nn.relu(input)
    return layer


def create_flatten_layer(layer):
    #We know that the shape of the layer will be [batch_size img_size img_size num_channels]
    # But let's get it from the previous layer.
    layer_shape = layer.get_shape()

    ## Number of features will be img_height * img_width* num_channels. But we shall calculate it in place of hard-coding it.
    num_features = layer_shape[1:4].num_elements()

    ## Now, we Flatten the layer so we shall have to reshape to num_features
    layer = tf.reshape(layer, [-1, num_features])

    return layer


def create_fc_layer(input, num_inputs, num_outputs, use_relu=True):

    #Let's define trainable weights and biases.
    weights = create_weights(shape=[num_inputs, num_outputs])
    biases = create_biases(num_outputs)

    # Fully connected layer takes input x and produces wx+b.Since, these are matrices, we use matmul function in Tensorflow
    layer = tf.matmul(input, weights) + biases
    if use_relu:
        layer = tf.nn.relu(layer)

    return layer


class netbuilder:
    def GenerateModel(self, design, predicting=False):
        self.net = 0

        for i in range(0, len(design)):
            d = design[i]
            if d[0] == INPUT:
                print('INPUT')
                self.net = tf.placeholder(
                    tf.float32, shape=[None, d[1], d[1], d[2]], name='x')
            elif d[0] == CONV2D:
                print('CONV2D')
                if i == 0:
                    print('INPUT', d[1], d[2], d[3])
                    self.net = create_convolutional_layer(
                        input=self.x,
                        num_input_channels=d[1],
                        conv_filter_size=d[2],
                        num_filters=d[3],
                        use_maxpool=d[4])
                else:
                    print(d)
                    self.net = create_convolutional_layer(
                        input=self.net,
                        num_input_channels=d[1],
                        conv_filter_size=d[2],
                        num_filters=d[3])
            elif d[0] == FLATTEN:
                print('FLATTEN')
                print(d)
                self.net = create_flatten_layer(self.net)
            elif d[0] == FULLYCONNECTED:
                print('FULLYCONNECTED')
                print(d)
                self.net = create_fc_layer(
                    self.net,
                    self.net.get_shape()[1:4].num_elements(),
                    d[1],
                    use_relu=d[2])

        self.y_pred = tf.nn.softmax(self.net, name='y_pred')
        self.y_pred_cls = tf.argmax(self.y_pred, dimension=1)
        self.y_true_cls = tf.argmax(self.y_true, dimension=1)
        self.session.run(tf.global_variables_initializer())
        self.cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
            logits=self.net, labels=self.y_true)
        self.cost = tf.reduce_mean(self.cross_entropy)
        if not predicting:
            self.optimizer = tf.train.AdamOptimizer(
                learning_rate=1e-4).minimize(self.cost)
        self.correct_prediction = tf.equal(self.y_pred_cls, self.y_true_cls)
        self.accuracy = tf.reduce_mean(
            tf.cast(self.correct_prediction, tf.float32))
        self.session.run(tf.global_variables_initializer())
        self.total_iterations = 0
        self.saver = tf.train.Saver()

    def GenerateInput(self, _image_size=160, _num_chanels=3, _classes=['',
                                                                       '']):
        self.classes = _classes
        self.num_classes = len(self.classes)
        # 20% of the data will automatically be used for validation
        self.img_size = _image_size
        self.num_channels = _num_chanels
        self.x = tf.placeholder(
            tf.float32,
            shape=[None, self.img_size, self.img_size, self.num_channels],
            name='x')
        self.y_true = tf.placeholder(
            tf.float32, shape=[None, self.num_classes], name='y_true')

    def GetImages(self,
                  folder='',
                  _image_size=160,
                  _num_chanels=3,
                  _classes=['', ''],
                  _validation_size=0.2,
                  num_samples=0):
        self.classes = _classes
        self.num_classes = len(self.classes)
        # 20% of the data will automatically be used for validation
        self.validation_size = _validation_size
        self.img_size = _image_size
        self.num_channels = _num_chanels
        self.x = tf.placeholder(
            tf.float32,
            shape=[None, self.img_size, self.img_size, self.num_channels],
            name='x')
        self.y_true = tf.placeholder(
            tf.float32, shape=[None, self.num_classes], name='y_true')
        self.y_true_cls = tf.argmax(self.y_true, dimension=1)
        self.data = tensorflow_networks.dataset.read_train_folders(
            folder, self.classes, self.validation_size, self.img_size,
            num_samples)

    def Load(self, filename):
        self.saver.restore(self.session, filename)
        print('load')

    def Save(self, filename):
        self.saver.save(self.session, './' + filename)
        print('save')

    def show_progress(self, epoch, step, feed_dict_train, feed_dict_validate,
                      val_loss):
        acc = self.session.run(self.accuracy, feed_dict=feed_dict_train)
        val_acc = self.session.run(self.accuracy, feed_dict=feed_dict_validate)
        msg = "Training Epoch {0} Step {1} --- Training Accuracy: {2:>6.1%}, Validation Accuracy: {3:>6.1%},  Validation Loss: {4:.3f}"
        print(msg.format(epoch + 1, step, acc, val_acc, val_loss))

    def Train(self, n_epochs=10, _batch_size=16, output_interval=50):
        self.batch_size = _batch_size
        print('train')
        step = 0
        epoch = 0
        while epoch < n_epochs:

            x_batch, y_true_batch, _, cls_batch = self.data.train.next_batch(
                self.batch_size)
            x_valid_batch, y_valid_batch, _, valid_cls_batch = self.data.valid.next_batch(
                self.batch_size)

            feed_dict_tr = {self.x: x_batch, self.y_true: y_true_batch}
            feed_dict_val = {self.x: x_valid_batch, self.y_true: y_valid_batch}

            self.session.run(self.optimizer, feed_dict=feed_dict_tr)

            #if i % int(self.data.train.num_examples/self.batch_size) == 0:
            val_loss = self.session.run(self.cost, feed_dict=feed_dict_val)
            epoch = int(
                step / int(self.data.train.num_examples / self.batch_size))
            if step % output_interval == 0:
                self.show_progress(epoch, step, feed_dict_tr, feed_dict_val,
                                   val_loss)
            step = step + 1

        self.total_iterations += 1
        self.saver.save(self.session, './TFN_SIMPLECNN.tfn')

    def predict(self, img):
        new_img = imresize(img, (self.img_size, self.img_size, 3))
        image = new_img.astype(np.float32)
        image = np.multiply(image, 1.0 / 255.0)
        feed_dict = {self.x: [image]}
        classification = self.session.run(self.y_pred, feed_dict)
        return classification

    def __init__(self):
        self.session = tf.Session()
        print('init')

        #Prepare input data