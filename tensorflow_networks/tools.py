from __future__ import division, print_function, absolute_import
from skimage import color, io
from scipy.misc import imresize
import numpy as np
from sklearn.model_selection import train_test_split
import os
from glob import glob
from tflearn.data_utils import shuffle, to_categorical
import json
from collections import OrderedDict

# Some tags used for building a neural network automatically
INPUT = 0
CONV2D = 1
FLATTEN = 2
FULLYCONNECTED = 3
MAXPOOL = 4
RELU = 5


def GetData(dir, offset, img_size, num_samples, label):
    images = np.zeros((num_samples, img_size, img_size, 3), dtype='float64')
    labels = np.zeros(num_samples)
    fst = offset
    count = 0
    for f in os.listdir(dir):
        if fst > 0:
            fst = fst - 1
        else:
            try:
                img = io.imread(dir + f)
                new_img = imresize(img, (img_size, img_size, 3))
                images[count] = np.array(new_img)
                labels[count] = label
                count += 1
            except:
                continue
        if count == num_samples:
            break
    return images, labels


def GetTrainingData(folders, img_size, num_samples):
    print('Get training data:', num_samples, 'images per folder. image size:',
          img_size)

    images = []
    labels = []

    index = 0

    for f in folders:
        if len(images) == 0:
            images, labels = GetData(f[0], 0, img_size, num_samples, f[1])
        else:
            _images, _labels = GetData(f[0], 0, img_size, num_samples, f[1])
            images = np.concatenate((images, _images), axis=0)
            labels = np.concatenate((labels, _labels), axis=0)

    images, labels = shuffle(images, labels)
    train_images, validation_images, train_labels, validation_labels = train_test_split(
        images, labels, test_size=0.2, random_state=42)
    train_labels = to_categorical(train_labels, 2)
    validation_labels = to_categorical(validation_labels, 2)
    return train_images, train_labels, validation_images, validation_labels


def netparser(filename, modelname):
    design = []
    with open(filename) as data_file:
        data = json.load(data_file, object_pairs_hook=OrderedDict)
        print(type(data))
        folders = []
        image_size = data["input"]["image_size"]
        classes = data["input"]["classes"]
        epochs = data["input"]["epochs"]
        num_samples = data["input"]["samples_per_folder"]
        for folder in data["input"]["data"]:
            folders.append((data["input"]["data"][folder]["path"],
                            data["input"]["data"][folder]["label"]))
        print('LOOKING FOR MODEL', modelname)
        #print (data[modelname])
        for element in data[modelname]:
            layer_type = data[modelname][element]["type"]
            if layer_type == "CONV2D":
                num_input_channels = data[modelname][element][
                    "num_input_channels"]
                filter_size = data[modelname][element]["conv_filter_size"]
                num_filters = data[modelname][element]["num_filters"]
                use_maxpool = data[modelname][element]["use_maxpool"]
                design.append((CONV2D, num_input_channels, filter_size,
                               num_filters, use_maxpool))
            elif layer_type == "FLATTEN":
                design.append((FLATTEN, 0))
            elif layer_type == "FULLYCONNECTED":
                num_outs = data[modelname][element]["num_outputs"]
                use_relu = data[modelname][element]["use_relu"]
                design.append((FULLYCONNECTED, num_outs, use_relu))
            elif layer_type == "MAXPOOL":
                ksize = data[modelname][element]["ksize"]
                strides = data[modelname][element]["strides"]
                design.append((MAXPOOL, ksize, strides))
            elif layer_type == 'RELU':
                print('RELU')
    print(design, image_size, classes, num_samples, folders)
    return design, image_size, classes, num_samples, folders, epochs

def netparser_2(inputs,networks, modelname):
    design = []

    with open(inputs) as data_file:
        data = json.load(data_file, object_pairs_hook=OrderedDict)
        print(type(data))
        folders = []
        image_size = data["input"]["image_size"]
        classes = data["input"]["classes"]
        epochs = data["input"]["epochs"]
        num_samples = data["input"]["samples_per_folder"]
        for folder in data["input"]["data"]:
            folders.append((data["input"]["data"][folder]["path"],
                            data["input"]["data"][folder]["label"]))

    with open(networks) as data_file_2:
        data = json.load(data_file_2, object_pairs_hook=OrderedDict)

        print('LOOKING FOR MODEL', modelname)
        #print (data[modelname])
        for element in data[modelname]:
            layer_type = data[modelname][element]["type"]
            if layer_type == "CONV2D":
                num_input_channels = data[modelname][element][
                    "num_input_channels"]
                filter_size = data[modelname][element]["conv_filter_size"]
                num_filters = data[modelname][element]["num_filters"]
                use_maxpool = data[modelname][element]["use_maxpool"]
                design.append((CONV2D, num_input_channels, filter_size,
                               num_filters, use_maxpool))
            elif layer_type == "FLATTEN":
                design.append((FLATTEN, 0))
            elif layer_type == "FULLYCONNECTED":
                num_outs = data[modelname][element]["num_outputs"]
                use_relu = data[modelname][element]["use_relu"]
                design.append((FULLYCONNECTED, num_outs, use_relu))
            elif layer_type == "MAXPOOL":
                ksize = data[modelname][element]["ksize"]
                strides = data[modelname][element]["strides"]
                design.append((MAXPOOL, ksize, strides))
            elif layer_type == 'RELU':
                print('RELU')

    print(design, image_size, classes, num_samples, folders)
    return design, image_size, classes, num_samples, folders, epochs
