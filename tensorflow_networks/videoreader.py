import cv2


class videoreader:
    def __init__(self):
        self.video = cv2.VideoCapture()

    def open(self, filename):
        self.video.open(filename, cv2.CAP_FFMPEG)
        return self.video.isOpened()

    def getframe(self):
        ret, frame = self.video.read()
        return frame

    def release(self):
        self.video.release()
