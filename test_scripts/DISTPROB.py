import os
import sys
import math
import csv
import sys
from pythonvideoannotator_models.models import Project
from math import factorial
import numpy as np

def savitzky_golay(a, window_size, order=0, deriv=0, rate=1):
    y = np.asarray(a)
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError as msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def get_path_point(sbjlist, sindex, pindex, findex):
    return sbjlist[sindex].datasets[pindex]._points[findex]

def get_prob_point(sbjlist, sindex, pindex, findex):
    return sbjlist[sindex].datasets[pindex]._values[findex]

def main(argv):
    project = Project()
    print(argv)
    project.load({} ,project_path=argv+'/videoannotator-project')
    video = project.videos[0]
    subjects = video.objects
    
    path_index = []
    exprob_index = []
    for subject in subjects:
        datasets = subject.datasets
        item_index = 0
        for item in datasets:
            if 'path' in item.FACTORY_FUNCTION:
                path_index.append(item_index)
                print('found path on item... index:',item_index)
            elif 'W100' in item.name:
                exprob_index.append(item_index)
                print('found W100 on item... index:',item_index)
                #store the path values so we can compute the distance
            item_index = item_index + 1
    
    for i in range(0,len(subjects)):
        pindex = path_index[i]
        eindex = exprob_index[i]

        path = subjects[i].datasets[pindex]._points
        probs = subjects[i].datasets[eindex]._values
        distances = []
        # check the closest distance to 
        for index in range(0,len(path)):
            dist = None
            for j in range(0,len(subjects)):
                if i != j:
                    pindex1 = path_index[j]
                    #eindex1 = exprob_index[j]
                    path1 = subjects[j].datasets[pindex1]._points
                    #print(index,len(path1),len(path))
                    if index < len(path1) and index < len(path):
                        if path[index] is not None and path1[index] is not None:
                            x1, y1 = path[index]
                            x2, y2 = path1[index]
                        else:
                            x1 = 1
                            y1 = 1
                            x2 = 5000
                            y2 = 5000
                        if dist is None:
                            dist = math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
                        else:
                            aux = math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
                            if aux > 0 and aux < dist:
                                dist = aux
            if dist is not None:
                distances.append(dist)
            else:
                distances.append(1000)

        print('distances len:',len(distances))
        minimmum = float(min(distances))
        maximum = float(max(distances))
        print('minimum',minimmum)
        print('maximum',maximum)

        mini = min(distances)
        maxi = max(distances)
        print('minimum',mini)
        print('maximum',maxi)
        probdist = []
        for m in range(0,len(probs)):
            if m < len(probs) and m < len(distances):
                dist = distances[m]
                probl = probs[m]
                finalprob = 0
                if dist < 150:
                    if finalprob > 0.7:
                        finalprob = probl + 0.75*(1-probl)
                    else:
                        finalprob = probl + 0.25*(1-probl)
                else:
                    finalprob = probl - 0.5*(1-probl)
                if dist < 50:
                    finalprob = 0
                if finalprob > 1:
                    finalprob = 1
                if finalprob < 0:
                    finalprob = 0

                probdist.append(finalprob)
                
        probdist = savitzky_golay(probdist,51)
        
        mini = min(probdist)
        maxi = max(probdist)

        print('minimum prob',mini)
        print('maximum prob',maxi)
                
        outfile = argv+'/temp'+str(i)+'.csv'
        with open(outfile,'w',newline='') as csvfile:
            print('writing csv file')
            fieldnames = ['Frame','prob']
            writer = csv.DictWriter(csvfile,fieldnames = fieldnames)
            writer.writeheader()
            idx = 0
            print('mini:',mini,'maxi',maxi)
            for prob in probdist:
                if maxi > 0:
                    aux = (prob-mini)/(maxi - mini)
                    writer.writerow({'Frame': idx,'prob': aux})
                #probs[n] = (probs[n]-mini)/(maxi - mini)
                else:
                    writer.writerow({'Frame': idx,'prob': 0})
                idx = idx+1

            
            val = subjects[i].create_value()
            val.name = 'Extrusion Prob DISTANCE'
            val.import_csv(outfile, first_row=1, col_frame=0, col_value=1)

    print('saving file')
    project.save({},argv+'/videoannotator-extended')
    print('done')


if __name__ == '__main__':
    main(sys.argv[1:])
    #main('C:/CODE/VIDEOS/video23_2017-05-18T11_09_38/videoannotator-project')
