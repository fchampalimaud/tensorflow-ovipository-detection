import os
import sys
import math
import csv
from pythonvideoannotator_models.models import Project
from math import factorial
import numpy as np

def savitzky_golay(a, window_size, order=0, deriv=0, rate=1):
    y = np.asarray(a)
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError as msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def get_path_point(sbjlist, sindex, pindex, findex):
    return sbjlist[sindex].datasets[pindex]._points[findex]

def get_prob_point(sbjlist, sindex, pindex, findex):
    return sbjlist[sindex].datasets[pindex]._values[findex]

def main(argv):
    project = Project()
    print(argv)
    project.load({} ,project_path=argv)
    video = project.videos[0]
    subjects = video.objects
    
    path_index = []
    exprob_index = []
    for subject in subjects:
        datasets = subject.datasets
        item_index = 0
        for item in datasets:
            if 'path' in item.FACTORY_FUNCTION:
                path_index.append(item_index)
                print('found path on item... index:',item_index)
            elif 'W100' in item.name:
                exprob_index.append(item_index)
                print('found W100 on item... index:',item_index)
                #store the path values so we can compute the distance
            item_index = item_index + 1
    
    for i in range(0,len(subjects)):
        pindex = path_index[i]
        eindex = exprob_index[i]

        path = subjects[i].datasets[pindex]._points
        probs = subjects[i].datasets[eindex]._values

        # check 
        for j in range(0,len(subjects)):
            if i != j:
                pindex1 = path_index[j]
                #eindex1 = exprob_index[j]
                path1 = subjects[j].datasets[pindex1]._points
                distances = []
                for index in range(0,len(path1)):
                    if index < len(path1) and index < len(path):
                        x1, y1 = path[index]
                        x2, y2 = path1[index]
                        distances.append(math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 ))
                print('distances len:',len(distances))
                minimmum = float(min(distances))
                maximum = float(max(distances))
                print('minimum',minimmum)
                print('maximum',maximum)
                # DISTANCE NORMALIZATION
                '''
                for k  in range(0,len(distances)):
                    distances[k] = 1-((distances[k] - minimmum)/(maximum - minimmum))
                '''
                #distances = (float(distances) - minimmum)/(maximum - minimmum)
                mini = min(distances)
                maxi = max(distances)
                print('minimum',mini)
                print('maximum',maxi)
                probdist = []
                for m in range(0,len(probs)):
                    if m < len(probs) and m < len(distances):
                        dist = distances[m]
                        probl = probs[m]
                        if dist < 150 and probl > 0.7: # OE event
                            if dist < 50:
                                probdist.append(0) # ATTEMPT COPULATION
                            else:
                                probdist.append(1)
                        if dist < 150 and probl < 0.7: # LESS PROBABLE
                            if dist < 50:
                                probdist.append(0) # ATTEMPT COPULATION
                            else:
                                probdist.append(0.75)
                        if dist > 150 and probl > 0.7: # NOT OE EVENTS
                            probdist.append(0)
                        if dist > 150 and probl < 0.7:
                            probdist.append(0)
                            
                        #probdist.append((distances[m]-0.9)*probs[m])
                
                for loop in range(0,10):
                    probdist = savitzky_golay(probdist,99)

                mini = min(probdist)
                maxi = max(probdist)

                print('minimum prob',mini)
                print('maximum prob',maxi)

                
        outfile = 'C:/CODE/VIDEOS/video23_2017-11-17T11_09_27/temp'+str(i)+'.csv'
        with open(outfile,'w',newline='') as csvfile:
            print('writing csv file')
            fieldnames = ['Frame','prob']
            writer = csv.DictWriter(csvfile,fieldnames = fieldnames)
            writer.writeheader()
            idx = 0
            for prob in probdist:
                aux = (prob-mini)/(maxi - mini)
                #probs[n] = (probs[n]-mini)/(maxi - mini)
                writer.writerow({'Frame': idx,'prob': aux})
                idx = idx+1

            
            val = subjects[i].create_value()
            val.name = 'Extrusion Prob DISTANCE'
            val.import_csv(outfile, first_row=1, col_frame=0, col_value=1)

    print('saving file')
    project.save({},'C:/CODE/VIDEOS/video23_2017-11-17T11_09_27/videoannotator-extended')
    print('done')



    '''
    for i in range(0,len(subjects)):
        pindex = path_index[i]
        eindex = exprob_index[i]

        numexpoints = len(subjects[i].datasets[exprob_index[i]]._values)
        for f in range(0,numexpoints - 1):
            for j in range(0,len(subjects)):
                if j != i:
                    print('FRAME',f)
                    x1, y1 = subjects[i].datasets[path_index[i]]._points[f]
                    print(x1, y1)
                    x2, y2 = subjects[j].datasets[path_index[j]]._points[f]
                    print(x2, y2)
                    dstc = math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
                    print('distance:', dstc)
    '''




if __name__ == '__main__':
    #main('C:/CODE/VIDEOS/videoM114_2017-11-09T19_36_59/videoannotator-project')
    main('C:/CODE/VIDEOS/video23_2017-11-17T11_09_27/videoannotator-project')
