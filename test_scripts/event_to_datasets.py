import csv
import os
import shutil


def read_events(file):
    print('Reading csv file', file)
    ext_list = []
    with open(file, 'U') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        numfiles = 0
        extrusion_events = False
        for row in spamreader:
            if len(row) > 0:
                if row[0] == 'T' and row[1] == 'OE':
                    extrusion_events = True
                if row[0] == 'T' and row[1] != 'OE':
                    extrusion_events = False
                if row[0] == 'P' and extrusion_events is True:
                    i = int(row[2])
                    j = int(row[3])
                    ext_list.append((i, j))
    return ext_list


def isextrusionevent(filename, extrusion_list):
    print('checking extrusion list')
    isevent = 0

    if filename.endswith('.png'):
        print(filename)
        base = os.path.basename(filename)
        framenumber = int(os.path.splitext(base)[0])

        for event in extrusion_list:
            if event[0] <= framenumber <= event[1]:
                isevent = 1
    else:
        isevent = -1  # it's not an image... don't copy

    return isevent


def main():
    print('Extrusion Dataset Creator')

    infolders = []

    out = 'D:/Datasets/extrusion/'
    out2 = 'D:/Datasets/noextrusion/'

    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video23_2017-05-18T11_09_38/images/contours-images/video23_2017-05-18T11_09_38/contours(1)/',
        'G:/O meu disco/OvipositorExtrusion/MA_video23.csv'))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video68_2017-05-29T10_54_29/Images/contours-images/video68_2017-05-29T10_54_29/contours(1)/',
        'G:/O meu disco/OvipositorExtrusion/MA_video68.csv'))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video73_2017-05-29T12_06_13/Images/contours-images/video73_2017-05-29T12_06_13/contours(1)/',
        'G:/O meu disco/OvipositorExtrusion/MA_video73.csv'))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video75_2017-05-29T12_35_34/Images/contours-images/video75_2017-05-29T12_35_34/contours(1)/',
        'G:/O meu disco/OvipositorExtrusion/MA_video75.csv'))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video77_2017-05-30T10_28_05/Images/contours-images/video77_2017-05-30T10_28_05/contours(1)/',
        'G:/O meu disco/OvipositorExtrusion/MA_video77.csv'))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video78_2017-05-30T10_32_35/Images/contours-images/video78_2017-05-30T10_32_35/contours(2)/',
        'G:/O meu disco/OvipositorExtrusion/MA_video78.csv'))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video79_2017-05-30T10_35_19/Images/contours-images/video79_2017-05-30T10_35_19/contours(1)/',
        'G:/O meu disco/OvipositorExtrusion/MA_video79.csv'))

    # this variables are used to check the number of files copied to each folder
    copied = 1
    copied2 = 1

    for folders in infolders:
        extrusion_list = read_events(folders[1])
        print(extrusion_list)
        for root, dirs, files in os.walk(folders[0]):
            for filename in files:
                # check if this file represents and extrusion event
                if isextrusionevent(filename, extrusion_list) == 1:
                    dst = out + str(copied) + '.png'
                    if os.path.isfile(dst):
                        os.remove(dst)
                    print('Copyinng EXTRUSION file', filename, 'to', dst)
                    shutil.copy2(folders[0] + filename, dst)
                    copied = copied + 1
                elif isextrusionevent(filename, extrusion_list) == 0:
                    dst = out2 + str(copied2) + '.png'
                    if os.path.isfile(dst):
                        os.remove(dst)
                    print('Copyinng EXTRUSION file', filename, 'to', dst)
                    shutil.copy2(folders[0] + filename, dst)
                    copied2 = copied2 + 1


if __name__ == '__main__':
    main()