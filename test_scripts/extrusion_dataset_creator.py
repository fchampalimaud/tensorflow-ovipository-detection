import csv
import os
import cv2
import shutil


def select_by_image():
    print('EXTRUSION DATASET BUILDER')

    infolders = []

    # Append all the image paths and the event csv's
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video21_2017-05-18T10_40_09/Images/',
        'G:/O meu disco/OvipositorExtrusion/MA_video21.csv'))
    outfolder = 'G:/O meu disco/DataSets/'

    for f in infolders:
        print('opening', f[1])
        with open(f[1], 'U') as csvfile:
            #dialect = csv.Sniffer().sniff(csvfile.read(1024))
            #csvfile.seek(0)
            spamreader = csv.reader(csvfile, delimiter=',')
            for row in spamreader:
                if len(row) > 0:
                    if row[0] == 'P':
                        i = int(row[2])
                        j = int(row[3])
                        numfiles = j - i
                        selected = 0
                        while (i < j):
                            file1 = f[0] + "contours-images/video21_2017-05-18T10_40_09/contours(1)/" + str(
                                i) + '.png'
                            file2 = f[0] + "contours-images/video21_2017-05-18T10_40_09/contours(2)/" + str(
                                i) + '.png'
                            if os.path.isfile(file1):
                                print(file1)
                            if os.path.isfile(file2):
                                print(file2)
                            im1 = cv2.imread(file1)
                            im2 = cv2.imread(file2)
                            if im1 is not None:
                                cv2.imshow('mosca1', im1)
                            if im2 is not None:
                                cv2.imshow('mosca2', im2)
                            key = cv2.waitKey(0)
                            print(key)
                            if key == 49:
                                selected = 1
                                break
                            if key == 50:
                                selected = 2
                                selected
                            i = i + 1
                        i = int(row[2])
                        # we selected our fly... try to copy the files to the dataset folder
                        while (i < j):
                            file1 = f[0] + "contours-images/video21_2017-05-18T10_40_09/contours(1)/" + str(
                                i) + '.png'
                            file2 = f[0] + "contours-images/video21_2017-05-18T10_40_09/contours(2)/" + str(
                                i) + '.png'
                            if os.path.isfile(file1) and selected == 1:
                                print(file1)
                            if os.path.isfile(file2) and selected == 2:
                                print(file2)
                            i = i + 1


# check if number is on the blacklist
def canCopy(index, blacklist):
    for entry in blacklist:
        if entry[0] <= index <= entry[1]:
            return False
    return True


def main_old():
    print('EXTRUSION DATASET CREATOR')

    infolders = []

    out = 'G:/O meu disco/DataSets/extrusion/'
    out2 = 'G:/O meu disco/DataSets/noextrusion/'

    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video23_2017-05-18T11_09_38/images/contours-images/video23_2017-05-18T11_09_38/contours(1)/',
        'G:/O meu disco/OvipositorExtrusion/MA_video23.csv'))

    copied = 1
    copied2 = 1

    for f in infolders:
        print('opening', f[1])
        extrusion_events = False
        numfiles = 0
        with open(f[1], 'U') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',')
            numfiles = 0
            not_to_copy = []
            for row in spamreader: 
                if len(row) > 0:
                    if row[0] == 'T' and row[1] == 'OE':
                        extrusion_events = True
                    if row[0] == 'T' and row[1] != 'OE':
                        extrusion_events = False
                    if row[0] == 'P' and extrusion_events is True:
                        i = int(row[2])
                        j = int(row[3])
                        not_to_copy.append((i, j))
                        numfiles = numfiles + (j - i)
                        while (i < j):
                            filename = f[0] + str(i) + '.png'
                            if os.path.isfile(filename):
                                dst = out + str(copied) + '.png'
                                if os.path.isfile(dst):
                                    os.remove(dst)
                                print('Copyinng EXTRUSION file', filename,
                                      'to', dst)
                                #shutil.copy2(filename, dst)
                                copied = copied + 1
                            i = i + 1
                            
            # we now need to get the same number of samples for the other folder
            x = 1
            while copied2 < copied:
                # check if the file we are about to copy can does not belong to an extrusion event
                if canCopy(x, not_to_copy) is True:
                    # prepare everything to make the copy
                    filename = f[0] + str(x) + '.png'
                    if os.path.isfile(filename):
                        # copy the file
                        dst = out2 + str(copied2) + '.png'
                        print('Copyinng NON EXTRUSION', filename, 'to', dst)
                        #shutil.copy2(filename, dst)
                        copied2 = copied2 + 1
                    x = x + 1

def main():
    print('EXTRUSION DATASET CREATOR')

    infolders = []

    out = 'G:/O meu disco/DataSets/extrusion/'
    out2 = 'G:/O meu disco/DataSets/noextrusion/'

    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video23_2017-05-18T11_09_38/images/contours-images/video23_2017-05-18T11_09_38/contours(1)/',
        'G:/O meu disco/OvipositorExtrusion/MA_video23.csv'))

    copied = 1
    copied2 = 1
    
    for folders in infolders:
        
        

if __name__ == '__main__':
    main()