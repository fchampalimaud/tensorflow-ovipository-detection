from tensorflow_networks import videoreader
from tensorflow_networks.native import tensorflow_net_builder
from tensorflow_networks import tools
import cv2
import csv
import copy
import math


def read_events(file):
    print('Reading csv file', file)
    ext_list = []
    with open(file, 'U') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        numfiles = 0
        extrusion_events = False
        for row in spamreader:
            if len(row) > 0:
                if row[0] == 'T' and row[1] == 'OE':
                    extrusion_events = True
                if row[0] == 'T' and row[1] != 'OE':
                    extrusion_events = False
                if row[0] == 'P' and extrusion_events is True:
                    i = int(row[2])
                    j = int(row[3])
                    ext_list.append((i, j))
    return ext_list


def isextrusionevent(framenumber, extrusion_list):
    isevent = 0
    for event in extrusion_list:
        if event[0] <= framenumber <= event[1]:
            isevent = 1
    return isevent


def readcsv(filename, startingrow):
    t1 = []
    t2 = []
    with open(filename, 'U') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)
        spamreader = csv.reader(csvfile, dialect)
        skiped = False
        for row in spamreader:
            if skiped == False:
                startingrow = startingrow - 1
                if startingrow == 0:
                    skiped = True
            else:
                t1.append((float(row[0]), float(row[1])))
                t2.append((float(row[3]), float(row[4])))

    return t1, t2


def main():
    '''
    This program aims for automatic extrusion event anotation using opencv for 
    video reading video files and tensorflow for classification of fly images
    '''

    videoname = 'video23_2017-05-18T11_09_38'
    cnnname = 'cnn3layer'
    eventfile = 'G:/O meu disco/OvipositorExtrusion/MA_video23.csv'

    event_list = read_events(eventfile)

    tp = 0
    tn = 0
    fp = 0
    fn = 0

    # we also need to get the detected paths using the csv generated from idtracker
    t1, t2 = readcsv('G:/O meu disco/OvipositorExtrusion/' + videoname +
                     '/trajectories_nogaps.txt', 1)

    video = videoreader.videoreader()
    result = video.open('G:/O meu disco/OvipositorExtrusion/' + videoname + '/'
                        + videoname + '.avi')
    if result:
        design, image_size, imgclasses, num_samples, data, epochs = tools.netparser(
            'networks.json', cnnname)
        '''
        cnn = tensorflow_net_builder.netbuilder()
        cnn.GenerateInput(160, 3, ["extrusion", "noextrusion"])
        cnn.GenerateModel(design)
        cnn.Load(cnnname + '.tf')
        '''
        cnn = tensorflow_net_builder.netbuilder()
        cnn.GenerateInput(160, 3, ["female", "male"])
        cnn.GenerateModel(design)
        cnn.Load(cnnname + '.tf')

        currentframe = 0
        while (1):
            videoframe = video.getframe()
            aux = copy.copy(videoframe)

            isevent = 0
            eventfound = False
            extrusiondetected = False
            if isextrusionevent(currentframe, event_list):
                isevent = 255
                eventfound = True

            if len(t1) > 0 and not math.isnan(
                    t1[currentframe][0]) and not math.isnan(
                        t1[currentframe][1]):
                roi1 = ((int(t1[currentframe][0]) - 80,
                         int(t1[currentframe][1]) - 80),
                        (int(t1[currentframe][0]) + 80,
                         int(t1[currentframe][1]) + 80))
                object1 = aux[roi1[0][1]:roi1[1][1], roi1[0][0]:roi1[1][0]]
                result1 = cnn.predict(object1)
                print(result1)
                cv2.imshow('mosca1', object1)
                if result1[0][0] > result1[0][1]:
                    cv2.rectangle(
                        videoframe,
                        roi1[0],
                        roi1[1], (0, isevent, 255 - isevent),
                        thickness=4)
                    extrusiondetected = True

                elif isevent > 0:
                    cv2.rectangle(
                        videoframe,
                        roi1[0],
                        roi1[1], (isevent, 0, 0),
                        thickness=4)

            if len(t2) > 0 and not math.isnan(
                    t2[currentframe][0]) and not math.isnan(
                        t2[currentframe][1]):
                roi2 = ((int(t2[currentframe][0]) - 80,
                         int(t2[currentframe][1]) - 80),
                        (int(t2[currentframe][0]) + 80,
                         int(t2[currentframe][1]) + 80))
                object2 = aux[roi2[0][1]:roi2[1][1], roi2[0][0]:roi2[1][0]]
                result2 = cnn.predict(object2)
                print(result2)
                cv2.imshow('mosca2', object2)
                if result2[0][0] > result2[0][1]:
                    cv2.rectangle(
                        videoframe,
                        roi2[0],
                        roi2[1], (0, isevent, 255 - isevent),
                        thickness=4)
                    extrusiondetected = True

                elif isevent > 0:
                    cv2.rectangle(
                        videoframe,
                        roi2[0],
                        roi2[1], (isevent, 0, 0),
                        thickness=4)

            if eventfound and extrusiondetected:
                tp = tp + 1
            elif eventfound and not extrusiondetected:
                fn = fn + 1
            elif not eventfound and extrusiondetected:
                fp = fp + 1
            elif not eventfound and not extrusiondetected:
                tn = tn + 1

            acc = 0
            precision = 0
            recall = 0
            tnrate = 0
            if tp > 0 or tn > 0 or fp > 0 or fn > 0:
                acc = (tp + tn) / (tp + tn + fp + fn)
            if tp > 0 or fp > 0:
                precision = tp / (tp + fp)
            if tp > 0 or fn > 0:
                recall = tp / (tp + fn)
            if tn > 0 or fp > 0:
                tnrate = tn / (tn + fp)

            print('true positives:', tp, 'false positives', fp,
                  'true negatives:', tn, 'false negatives:', fn, 'accuracy:',
                  round(acc, 2), 'precision:',
                  round(precision, 2), 'recall:',
                  round(recall, 2), 'true negative rate:', round(tnrate, 2))

            cv2.imshow('frame', videoframe)
            key = cv2.waitKey(1)
            if key == 99:
                break
            currentframe = currentframe + 1


if __name__ == '__main__':
    main()