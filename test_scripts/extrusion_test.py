from tensorflow_networks import videoreader
from tensorflow_networks.native import tensorflow_net_builder
from tensorflow_networks import tools
import cv2
import csv
import copy
import math
from skimage import color, io


def main():
    '''
    This program aims for automatic extrusion event anotation using opencv for 
    video reading video files and tensorflow for classification of fly images
    '''

    folder = "G:/O meu disco/DataSets/extrusion/"

    # we also need to get the detected paths using the csv generated from idtracker
    design, image_size, imgclasses, num_samples, data, epochs = tools.netparser(
        'networks.json', 'cnn3layerV2')
    cnn = tensorflow_net_builder.netbuilder()
    cnn.GenerateInput(160, 3, ["extrusion", "noextrusion"])
    cnn.GenerateModel(design)
    cnn.Load('cnn3layer.tf')

    total = 0

    for i in range(1, 1000):
        #image = cv2.imread(folder + str(i) + '.png')
        image = io.imread(folder + str(i) + '.png')

        result = cnn.predict(image)
        total = total + result
        print(result)

    #print('total:', total)


if __name__ == '__main__':
    main()