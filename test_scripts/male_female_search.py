import csv
import os
import shutil
import cv2
import copy
import math
from tensorflow_networks import videoreader


def readcsv(filename, startingrow):
    t1 = []
    t2 = []
    with open(filename, 'U') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)
        spamreader = csv.reader(csvfile, dialect)
        skiped = False
        for row in spamreader:
            if skiped == False:
                startingrow = startingrow - 1
                if startingrow == 0:
                    skiped = True
            else:
                t1.append((float(row[0]), float(row[1])))
                t2.append((float(row[3]), float(row[4])))

    return t1, t2


def read_events(file):
    print('Reading csv file', file)
    ext_list = []
    with open(file, 'U') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        numfiles = 0
        extrusion_events = False
        for row in spamreader:
            if len(row) > 0:
                if row[0] == 'T' and row[1] == 'OE':
                    extrusion_events = True
                if row[0] == 'T' and row[1] != 'OE':
                    extrusion_events = False
                if row[0] == 'P' and extrusion_events is True:
                    i = int(row[2])
                    j = int(row[3])
                    ext_list.append((i, j))
    return ext_list


def isextrusionevent(framenumber, extrusion_list):

    isevent = 0

    for event in extrusion_list:
        if event[0] <= framenumber <= event[1]:
            isevent = 1
            break

    return isevent


def mfsearch_images():
    print('Extrusion Dataset Creator')

    infolders = []

    out = 'D:/Datasets/female/'
    out2 = 'D:/Datasets/male/'

    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video23_2017-05-18T11_09_38/images/contours-images/video23_2017-05-18T11_09_38/contours(1)/',
        0))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video23_2017-05-18T11_09_38/images/contours-images/video23_2017-05-18T11_09_38/contours(2)/',
        1))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video68_2017-05-29T10_54_29/Images/contours-images/video68_2017-05-29T10_54_29/contours(1)/',
        0))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video68_2017-05-29T10_54_29/Images/contours-images/video68_2017-05-29T10_54_29/contours(2)/',
        1))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video73_2017-05-29T12_06_13/Images/contours-images/video73_2017-05-29T12_06_13/contours(1)/',
        0))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video73_2017-05-29T12_06_13/Images/contours-images/video73_2017-05-29T12_06_13/contours(2)/',
        1))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video75_2017-05-29T12_35_34/Images/contours-images/video75_2017-05-29T12_35_34/contours(1)/',
        0))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video75_2017-05-29T12_35_34/Images/contours-images/video75_2017-05-29T12_35_34/contours(2)/',
        1))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video77_2017-05-30T10_28_05/Images/contours-images/video77_2017-05-30T10_28_05/contours(1)/',
        0))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video77_2017-05-30T10_28_05/Images/contours-images/video77_2017-05-30T10_28_05/contours(2)/',
        1))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video78_2017-05-30T10_32_35/Images/contours-images/video78_2017-05-30T10_32_35/contours(2)/',
        0))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video78_2017-05-30T10_32_35/Images/contours-images/video78_2017-05-30T10_32_35/contours(1)/',
        1))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video79_2017-05-30T10_35_19/Images/contours-images/video79_2017-05-30T10_35_19/contours(1)/',
        0))
    infolders.append((
        'G:/O meu disco/OvipositorExtrusion/video79_2017-05-30T10_35_19/Images/contours-images/video79_2017-05-30T10_35_19/contours(2)/',
        1))

    # this variables are used to check the number of files copied to each folder
    copied = 1
    copied2 = 1
    filesperfolder = 200
    for folders in infolders:
        totalcopied = 0
        for root, dirs, files in os.walk(folders[0]):
            for filename in files:
                # check if this file represents and extrusion event
                if folders[1] == 0:  #we have a female
                    dst = out + str(copied) + '.png'
                    if os.path.isfile(dst):
                        os.remove(dst)
                    print('Copyinng FEMALE', filename, 'to', dst)
                    shutil.copy2(folders[0] + filename, dst)
                    copied = copied + 1
                    totalcopied = totalcopied + 1
                elif folders[1] == 1:  # we have a male
                    dst = out2 + str(copied2) + '.png'
                    if os.path.isfile(dst):
                        os.remove(dst)
                    print('Copyinng MALE', filename, 'to', dst)
                    shutil.copy2(folders[0] + filename, dst)
                    copied2 = copied2 + 1
                    totalcopied = totalcopied + 1
                if totalcopied == filesperfolder:
                    break


def mfsearch_video():

    infolders = []

    out = 'D:/Datasets/female/'
    exout = 'D:/Datasets/extrusion3/'
    out2 = 'D:/Datasets/male/'

    startingnumber = 1

    copied1 = 0
    copied2 = 0
    excopied = 0

    infolders.append(('video21_2017-05-18T10_40_09', 'MA_video21.csv'))
    infolders.append(('video23_2017-05-18T11_09_38', 'MA_video23.csv'))
    infolders.append(('video68_2017-05-29T10_54_29', 'MA_video68.csv'))
    infolders.append(('video73_2017-05-29T12_06_13', 'MA_video73.csv'))
    infolders.append(('video75_2017-05-29T12_35_34', 'MA_video75.csv'))
    infolders.append(('video77_2017-05-30T10_28_05', 'MA_video77.csv'))
    infolders.append(('video78_2017-05-30T10_32_35', 'MA_video78.csv'))
    infolders.append(('video79_2017-05-30T10_35_19', 'MA_video79.csv'))

    video = videoreader.videoreader()

    for videoname in infolders:
        # we also need to get the detected paths using the csv generated from idtracker
        t1, t2 = readcsv('G:/O meu disco/OvipositorExtrusion/' + videoname[0] +
                         '/trajectories_nogaps.txt', 1)

        video.release()

        print('opening', videoname[0])

        result = video.open('G:/O meu disco/OvipositorExtrusion/' +
                            videoname[0] + '/' + videoname[0] + '.avi')

        eventlist = read_events('G:/O meu disco/OvipositorExtrusion/' +
                                videoname[1])

        if result:
            print('VIDEO OPENED')
            startingnumber = startingnumber + copied1
            copied1 = 0
            copied2 = 0
            currentframe = 0
            selected = False
            femaleindex = 0
            x1 = 0
            y1 = 0
            x2 = 0
            y2 = 0
            while (1):
                videoframe = video.getframe()
                if videoframe is None:
                    break
                aux = copy.copy(videoframe)
                t1updated = False
                t2updated = False
                if len(t1) > 0 and not math.isnan(
                        t1[currentframe][0]) and not math.isnan(
                            t1[currentframe][1]):
                    x1 = t1[currentframe][0]
                    y1 = t1[currentframe][1]
                    t1updated = True
                    roi1 = ((int(t1[currentframe][0]) - 80,
                             int(t1[currentframe][1]) - 80),
                            (int(t1[currentframe][0]) + 80,
                             int(t1[currentframe][1]) + 80))
                    object1 = aux[roi1[0][1]:roi1[1][1], roi1[0][0]:roi1[1][0]]
                if len(t2) > 0 and not math.isnan(
                        t2[currentframe][0]) and not math.isnan(
                            t2[currentframe][1]):
                    x2 = t2[currentframe][0]
                    y2 = t2[currentframe][1]
                    t2updated = True
                    roi2 = ((int(t2[currentframe][0]) - 80,
                             int(t2[currentframe][1]) - 80),
                            (int(t2[currentframe][0]) + 80,
                             int(t2[currentframe][1]) + 80))
                    object2 = aux[roi2[0][1]:roi2[1][1], roi2[0][0]:roi2[1][0]]
                if not selected:
                    cv2.rectangle(
                        videoframe, roi2[0], roi2[1], (0, 0, 255), thickness=4)
                    cv2.rectangle(
                        videoframe, roi1[0], roi1[1], (0, 255, 0), thickness=4)
                    cv2.imshow('frame', videoframe)
                    print('Green is object 1, red is object 2')
                    key = cv2.waitKey(0)
                    print(key)
                    if key == 49:
                        selected = True
                        femaleindex = 1
                    elif key == 50:
                        selected = True
                        femaleindex = 2
                elif selected:
                    distance = math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) *
                                         (y2 - y1))
                    if femaleindex == 2:  # and distance > 160:

                        if t2updated:
                            if isextrusionevent(
                                    currentframe, eventlist
                            ) == 1:  #copy this to the extrusion folder instead of the female folder
                                cv2.rectangle(
                                    videoframe,
                                    roi2[0],
                                    roi2[1], (255, 0, 0),
                                    thickness=4)
                                cv2.imwrite(
                                    exout + str(startingnumber + excopied) +
                                    '.png', object2)
                                excopied = excopied + 1
                            '''
                            else:
                                cv2.rectangle(
                                    videoframe,
                                    roi2[0],
                                    roi2[1], (0, 255, 0),
                                    thickness=4)
                                cv2.imwrite(out + str(startingnumber + copied1)
                                            + '.png', object2)
                                copied1 = copied1 + 1
                            
                        if t1updated:
                            cv2.imwrite(
                                out2 + str(startingnumber + copied1) + '.png',
                                object1)
                            copied2 = copied2 + 1
                        '''
                    if femaleindex == 1:  # and distance > 160:

                        if t1updated:
                            if isextrusionevent(
                                    currentframe, eventlist
                            ) == 1:  #copy this to the extrusion folder instead of the female folder
                                cv2.rectangle(
                                    videoframe,
                                    roi1[0],
                                    roi1[1], (255, 0, 0),
                                    thickness=4)
                                cv2.imwrite(
                                    exout + str(startingnumber + excopied) +
                                    '.png', object1)
                                excopied = excopied + 1
                            '''
                            else:
                                cv2.rectangle(
                                    videoframe,
                                    roi1[0],
                                    roi1[1], (0, 255, 0),
                                    thickness=4)
                                cv2.imwrite(out + str(startingnumber + copied1)
                                            + '.png', object1)
                                copied1 = copied1 + 1
                        if t2updated:
                            cv2.imwrite(
                                out2 + str(startingnumber + copied1) + '.png',
                                object2)
                            copied2 = copied2 + 1
                        '''
                    cv2.imshow('frame', videoframe)
                    key = cv2.waitKey(1)
                    if key == 99:
                        break

                currentframe = currentframe + 1


if __name__ == '__main__':
    mfsearch_video()