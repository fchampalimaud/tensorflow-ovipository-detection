from tensorflow_networks.native import cnn_N_layer

def main():
    #D:\CODE\Videos\video36_2017-05-26T10_19_49\Images\contours-images\video36_2017-05-26T10_19_49
    print('starting neural network')
    imgclasses = ['mosca1','mosca2']
    image_size = 160
    num_samples = 100
    data = []
    data.append(('D:/CODE/Videos/video36_2017-05-26T10_19_49/Images/contours-images/video36_2017-05-26T10_19_49/mosca1/',0))
    data.append(('D:/CODE/Videos/video36_2017-05-26T10_19_49/Images/contours-images/video36_2017-05-26T10_19_49/mosca2/',1))
    data.append(('D:/CODE/Videos/video88_2017-05-30T12_01_30/imgs/contours-images/video88_2017-05-30T12_01_30/contours(1)/',1))
    data.append(('D:/CODE/Videos/video88_2017-05-30T12_01_30/imgs/contours-images/video88_2017-05-30T12_01_30/contours(2)/',0))
    model = cnn_N_layer.cnn_N_layer()
    model.GetImages(data,160,3,imgclasses,0.2,num_samples)
    print('images read')
    model.GenerateModel()
    model.Train(n_epochs = 10,_batch_size = 16, info_interval = 1)
    model.Save('native_cnn2layer.tfn')
  
if __name__ == '__main__':
    main()