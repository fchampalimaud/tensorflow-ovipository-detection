import json
from collections import OrderedDict

def netparser(filename,modelname):
    with open(filename) as data_file:    
        data = json.load(data_file,object_pairs_hook=OrderedDict)
        print(type(data))
        print('LOOKING FOR MODEL',modelname)
        #print (data[modelname])
        for element in data[modelname]:
            print(data[modelname][element]["type"])

if __name__ == '__main__':
    netparser('networks.json','cnn3layer')