import sys
import cv2
import csv
import copy
import math
import fnmatch
import numpy as np
from math import factorial
from tensorflow_networks import videoreader
from tensorflow_networks.native import tensorflow_net_builder
from tensorflow_networks import tools

from pythonvideoannotator_models.models import Project
import os

import tailfinder

def savitzky_golay(a, window_size, order=0, deriv=0, rate=1):
    y = np.asarray(a)
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError as msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def read_tracker(filename, startingrow):
    t = []
    with open(filename, 'U') as csvfile:
        #dialect = csv.Sniffer().sniff(csvfile.read(2048),delimiters=',')
        csvfile.seek(0)
        #spamreader = csv.reader(csvfile, dialect)
        spamreader = csv.reader(csvfile,dialect='excel')
        skiped = False
        for row in spamreader:
            if skiped == False:
                startingrow = startingrow - 1
                if startingrow == 0:
                    skiped = True
            else:
                t.append((float(row[0]), float(row[1])))
    return t

def main(argv):
    # store all the input values
    input_file = argv[0]
    network_file = argv[1]
    network_name = argv[2]
    network_weights = argv[3]
    video_name = argv[4]
    # We might not need the last two args... test it

    # Structure containing all the subject data for project creation
    subjects = []
    
    canproceed = False

    # Search for all subject data we can find
    for dirpath, dirnames, filenames in os.walk("."):
        for file in filenames:
            if fnmatch.fnmatch(file, '*.csv'):
                filepath = dirpath[2:]+'/'+file
                print(filepath)
                path = read_tracker(filepath,1)
                auxarr = []
                obj = {'filename': file, 'filepath': filepath, 'path': path, 'extrusion_prob':auxarr}
                subjects.append(obj)
                canproceed = True

    if canproceed == False:
        project = Project()
        # add the video file
        annotator_video = project.create_video()
        annotator_video.filepath = os.path.basename(video_name)
        project.save({}, 'videoannotator-project' )
        return

    num_entries = 0

    for subject in subjects:
        print(len(subjects))
        print(subject['filename'])
        # get the max number of frames to be processed
        if num_entries == 0:
            num_entries = len(subject['path'])
        if num_entries > len(subject['path']):
            num_entries = len(subject['path'])
    
    print(input_file,network_file,network_name)

    # gather information about the network we're building
    design, image_size, imgclasses, num_samples, data, epochs = tools.netparser_2(input_file,
                                                                                  network_file, 
                                                                                  network_name)
    
    print('laoding network')

    cnn = tensorflow_net_builder.netbuilder()
    cnn.GenerateInput(image_size, 3, ["extrusion", "noextrusion"])
    cnn.GenerateModel(design)
    cnn.Load(network_weights + '.tf')

    video = videoreader.videoreader()

    video.release()

    video_ok = video.open(video_name)

    if video_ok:

        currentframe = 0

        while (1):
            
            videoframe = None
            videoframe = video.getframe()
            
            if videoframe is None or currentframe > num_entries-1:
                # build the PAV project and finish the job
                print('writing PAV file')
                #base for the project
                project = Project()
                # add the video file
                annotator_video = project.create_video()
                annotator_video.filepath = os.path.basename(video_name)

                for subject in subjects:
                    subject_name = os.path.splitext(subject['filename'])[0]
                    vaobj = annotator_video.create_object()
                    vaobj.name = subject_name
                    vatrack = vaobj.create_path()
                    vatrack.import_csv(subject['filepath'], first_row=1, col_frame=-1, col_x=0,col_y=1,dialect='excel')
                    # add the RAW tensorflow result
                    outfile = 'tf_result_'+subject['filename']
                    with open(outfile,'w') as csvfile:
                        fieldnames = ['Frame','R1_0','R1_1']
                        writer = csv.DictWriter(csvfile,fieldnames = fieldnames)
                        writer.writeheader()
                        i = 0
                        for item in subject['extrusion_prob']:
                            writer.writerow({'Frame': i,'R1_0': item})
                            i = i+1
                        vaval = vaobj.create_value()
                        vaval.name = 'Extrusion Prob RAW'
                        vaval.import_csv(outfile, first_row=1, col_frame=0, col_value=1)
                    # and get the filtered value
                    filtered = savitzky_golay(subject['extrusion_prob'],99)
                    outfile2 = 'tf_result_W100'+subject['filename']
                    with open(outfile2,'w') as csvfile:
                        fieldnames = ['Frame','R1_0','R1_1']
                        writer = csv.DictWriter(csvfile,fieldnames = fieldnames)
                        writer.writeheader()
                        i = 0
                        for item in filtered:
                            writer.writerow({'Frame': i,'R1_0': item})
                            i = i + 1
                        vaval2 = vaobj.create_value()
                        vaval2.name = 'Extrusion Prob W100'
                        vaval2.import_csv(outfile2, first_row=1, col_frame=0, col_value=1)
                
                project.save({}, 'videoannotator-project' )
                break

            else:

                aux = copy.copy(videoframe)

                for subject in subjects:

                    result = []
                    result.append((0,0))
                    if not math.isnan(subject['path'][currentframe][0]) and not math.isnan(subject['path'][currentframe][1]) and subject['path'][currentframe][0] > 80 and subject['path'][currentframe][1] > 80:
                        pt = (int(subject['path'][currentframe][0]),int(subject['path'][currentframe][1]))
                        print(pt)
                        roi1 = ((pt[0] - 80,
                                pt[1] - 80),
                                (pt[0] + 80,
                                pt[1] + 80))
                        object1 = None
                        img = None
                        print(roi1)
                        object1 = aux[roi1[0][1]:roi1[1][1], roi1[0][0]:roi1[1][0]]
                        if object1 is not None:
                            print(object1.shape)
                            img = tailfinder.tailfinder_image(object1)
                        if img is not None:
                            wh = img.shape[:2]
                            if wh[0] > 0 and wh[1] > 0:
                                print('shape:',img.shape[:2])
                                result = cnn.predict(img)
                    subject['extrusion_prob'].append(result[0][0])
                
                currentframe = currentframe + 1

        print('processing done')

    print('Exiting')

if __name__ == '__main__':
    main(sys.argv[1:])