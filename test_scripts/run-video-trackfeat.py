import sys
import cv2
import csv
import copy
import math
from tensorflow_networks import videoreader
from tensorflow_networks.native import tensorflow_net_builder
from tensorflow_networks import tools

from pythonvideoannotator_models.models import Project
import os

import tailfinder

def read_events(file):
    print('Reading csv file', file)
    ext_list = []
    with open(file, 'U') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        numfiles = 0
        extrusion_events = False
        for row in spamreader:
            if len(row) > 0:
                if row[0] == 'T' and row[1] == 'OE':
                    extrusion_events = True
                if row[0] == 'T' and row[1] != 'OE':
                    extrusion_events = False
                if row[0] == 'P' and extrusion_events is True:
                    i = int(row[2])
                    j = int(row[3])
                    ext_list.append((i, j))
    return ext_list


def isextrusionevent(framenumber, extrusion_list):
    isevent = 0
    for event in extrusion_list:
        if event[0] <= framenumber <= event[1]:
            isevent = 1
    return isevent

def read_tracker(filename, startingrow):
    t = []
    with open(filename, 'U') as csvfile:
        #dialect = csv.Sniffer().sniff(csvfile.read(2048),delimiters=',')
        csvfile.seek(0)
        #spamreader = csv.reader(csvfile, dialect)
        spamreader = csv.reader(csvfile,dialect='excel')
        skiped = False
        for row in spamreader:
            if skiped == False:
                startingrow = startingrow - 1
                if startingrow == 0:
                    skiped = True
            else:
                t.append((float(row[0]), float(row[1])))
    return t

def readcsv(filename, startingrow):
    t1 = []
    t2 = []
    with open(filename, 'U') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)
        spamreader = csv.reader(csvfile, dialect)
        skiped = False
        for row in spamreader:
            if skiped == False:
                startingrow = startingrow - 1
                if startingrow == 0:
                    skiped = True
            else:
                t1.append((float(row[0]), float(row[1])))
                t2.append((float(row[3]), float(row[4])))

    return t1, t2

def main(argv):

    print('init')

    input_file = argv[0]
    network_file = argv[1]
    network_name = argv[2]
    network_weights = argv[3]
    video_name = argv[4]
    trajectories_file = argv[5]
    output_file = argv[6]

    result_list = []


    

    print('loading video')

    video = videoreader.videoreader()

    print(video)

    design, image_size, imgclasses, num_samples, data, epochs = tools.netparser_2(input_file,
                                                                                  network_file, 
                                                                                  network_name)

    print('laoding network')

    cnn = tensorflow_net_builder.netbuilder()
    cnn.GenerateInput(image_size, 3, ["extrusion", "noextrusion"])
    cnn.GenerateModel(design)
    cnn.Load(network_weights + '.tf')

    # we also need to get the detected paths using the csv generated from idtracker
    t1 = read_tracker(trajectories_file, 1)

    video.release()

    video_ok = video.open(video_name)

    print('video ok:', video_ok)

    if video_ok:
        print('VIDEO OPEN... START ANALYSING')
        currentframe = 0
        while (1):
                videoframe = None
                videoframe = video.getframe()
                if videoframe is None or currentframe > len(t1)-1:
                    # we shall wite the csv file here
                   
                    with open(output_file ,'w') as csvfile:
                        fieldnames = ['Frame','R1_0','R1_1']
                        writer = csv.DictWriter(csvfile,fieldnames = fieldnames)
                        writer.writeheader()
                        for item in result_list:
                            writer.writerow({'Frame': item[0],'R1_0': item[1],'R1_1': item[2]})

                        project = Project()
                        annotator_video = project.create_video()
                        annotator_video.filepath = os.path.basename(video_name)
                        annotator_obj = annotator_video.create_object()
                        annotator_obj.name = 'female'
                        annotator_val = annotator_obj.create_value()
                        annotator_val.name = 'tensorflow-results'
                        annotator_val.import_csv(output_file, first_row=1, col_frame=0, col_value=1)

                        #annotator_path = annotator_obj.create_path()
                        #annotator_path.import_csv(trajectories_file, first_row=1, col_frame=-1, col_x=0, col_y=1)
                        #annotator_path.name = 'trajectory-{0}'.format(obj_n)

                        project.save({}, 'videoannotator-project' )
                        
                    break

                aux = copy.copy(videoframe)

                result1 = []
                result1.append((0,0))

                if len(t1) > 0 and not math.isnan(
                        t1[currentframe][0]) and not math.isnan(
                            t1[currentframe][1]):
                    roi1 = ((int(t1[currentframe][0]) - 80,
                            int(t1[currentframe][1]) - 80),
                            (int(t1[currentframe][0]) + 80,
                            int(t1[currentframe][1]) + 80))
                    object1 = aux[roi1[0][1]:roi1[1][1], roi1[0][0]:roi1[1][0]]
                    img = tailfinder.tailfinder_image(object1)
                    
                    if img is not None:
                        wh = img.shape[:2]
                        if wh[0] > 0 and wh[1] > 0:
                            print('shape:',img.shape[:2])
                            result1 = cnn.predict(img)
                            result_list.append((currentframe,result1[0][0],result1[0][1]))

                currentframe = currentframe + 1
    
    print('Exiting')

if __name__ == '__main__':
    '''
    This shall receive as arguments the input json, the network json and the network name
    '''
    main(sys.argv[1:])
