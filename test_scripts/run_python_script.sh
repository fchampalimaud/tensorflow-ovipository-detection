universe = vanilla
executable = /usr/bin/python3

InitialDir= /cluster/vasconcelos/vasconcelos2/tensorflow-dnn

getenv = true
stream_output = true
request_cpus = 1
request_memory = 1024
request_gpus = 1
environment = "LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64"

transfer_input_files = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/Code, /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/Datasets/extrusion3, /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/Datasets/female

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_1layer.json cnn1layer_8_mp cnn1layer_8_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_1layer.json cnn1layer_16_mp cnn1layer_16_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_1layer.json cnn1layer_32_mp cnn1layer_32_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_1layer.json cnn1layer_64_mp cnn1layer_64_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_1layer.json cnn1layer_8_mp_1fc cnn1layer_8_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_1layer.json cnn1layer_16_mp_1fc cnn1layer_16_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_1layer.json cnn1layer_32_mp_1fc cnn1layer_32_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_1layer.json cnn1layer_64_mp_1fc cnn1layer_64_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_2layer.json cnn2layer_8_mp cnn2layer_8_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_2layer.json cnn2layer_16_mp cnn2layer_16_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_2layer.json cnn2layer_32_mp cnn2layer_32_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_2layer.json cnn2layer_64_mp cnn2layer_64_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_2layer.json cnn2layer_8_mp_1fc cnn2layer_8_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_2layer.json cnn2layer_16_mp_1fc cnn2layer_16_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_2layer.json cnn2layer_32_mp_1fc cnn2layer_32_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_2layer.json cnn2layer_64_mp_1fc cnn2layer_64_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_3layer.json cnn3layer_8_mp cnn3layer_8_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_3layer.json cnn3layer_16_mp cnn3layer_16_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_3layer.json cnn3layer_32_mp cnn3layer_32_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_3layer.json cnn3layer_64_mp cnn3layer_64_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_3layer.json cnn3layer_8_mp_1fc cnn3layer_8_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_3layer.json cnn3layer_16_mp_1fc cnn3layer_16_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_3layer.json cnn3layer_32_mp_1fc cnn3layer_32_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_3layer.json cnn3layer_64_mp_1fc cnn3layer_64_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_4layer.json cnn4layer_8_mp cnn4layer_8_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_4layer.json cnn4layer_16_mp cnn4layer_16_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_4layer.json cnn4layer_32_mp cnn4layer_32_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_4layer.json cnn4layer_64_mp cnn4layer_64_mp_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_4layer.json cnn4layer_8_mp_1fc cnn4layer_8_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_4layer.json cnn4layer_16_mp_1fc cnn4layer_16_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_4layer.json cnn4layer_32_mp_1fc cnn4layer_32_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_1fc_in1.log

queue

arguments = Code/neural_net_trainer.py Code/input_1.json Code/extrusion_networks_4layer.json cnn4layer_64_mp_1fc cnn4layer_64_mp_1fc_in1
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_1fc_in1.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_1fc_in1.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_1fc_in1.log

queue