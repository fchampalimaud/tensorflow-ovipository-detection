universe = vanilla
executable = /usr/bin/python3

InitialDir= /cluster/vasconcelos/vasconcelos2/tensorflow-dnn

getenv = true
stream_output = true
request_cpus = 1
request_memory = 1024
request_gpus = 1
environment = "LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64"

transfer_input_files = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/Code, /cluster/vasconcelos/vasconcelos2/cecilia/movies/20170529/video68_2017-05-29T10_54_29, /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/Nets

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_1layer.json cnn1layer_8_mp Nets/cnn1layer_8_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_8_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_1layer.json cnn1layer_16_mp Nets/cnn1layer_16_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_16_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_1layer.json cnn1layer_32_mp Nets/cnn1layer_32_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_32_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_1layer.json cnn1layer_64_mp Nets/cnn1layer_64_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_64_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_1layer.json cnn1layer_8_mp_1fc Nets/cnn1layer_8_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_1layer.json cnn1layer_16_mp_1fc Nets/cnn1layer_16_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_1layer.json cnn1layer_32_mp_1fc Nets/cnn1layer_32_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_1layer.json cnn1layer_64_mp_1fc Nets/cnn1layer_64_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_2layer.json cnn2layer_8_mp Nets/cnn2layer_8_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_8_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_2layer.json cnn2layer_16_mp Nets/cnn2layer_16_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_16_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_2layer.json cnn2layer_32_mp Nets/cnn2layer_32_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_32_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_2layer.json cnn2layer_64_mp Nets/cnn2layer_64_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_64_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_2layer.json cnn2layer_8_mp_1fc Nets/cnn2layer_8_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_2layer.json cnn2layer_16_mp_1fc Nets/cnn2layer_16_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_2layer.json cnn2layer_32_mp_1fc Nets/cnn2layer_32_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_2layer.json cnn2layer_64_mp_1fc Nets/cnn2layer_64_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_3layer.json cnn3layer_8_mp Nets/cnn3layer_8_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_8_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_3layer.json cnn3layer_16_mp Nets/cnn3layer_16_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_16_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_3layer.json cnn3layer_32_mp Nets/cnn3layer_32_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_32_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_3layer.json cnn3layer_64_mp Nets/cnn3layer_64_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_64_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_3layer.json cnn3layer_8_mp_1fc Nets/cnn3layer_8_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_3layer.json cnn3layer_16_mp_1fc Nets/cnn3layer_16_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_3layer.json cnn3layer_32_mp_1fc Nets/cnn3layer_32_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_3layer.json cnn3layer_64_mp_1fc Nets/cnn3layer_64_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_4layer.json cnn4layer_8_mp Nets/cnn4layer_8_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_8_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_4layer.json cnn4layer_16_mp Nets/cnn4layer_16_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_16_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_4layer.json cnn4layer_32_mp Nets/cnn4layer_32_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_32_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_4layer.json cnn4layer_64_mp Nets/cnn4layer_64_mp_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_64_mp_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_4layer.json cnn4layer_8_mp_1fc Nets/cnn4layer_8_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_4layer.json cnn4layer_16_mp_1fc Nets/cnn4layer_16_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_4layer.json cnn4layer_32_mp_1fc Nets/cnn4layer_32_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_3.json Code/extrusion_networks_4layer.json cnn4layer_64_mp_1fc Nets/cnn4layer_64_mp_1fc_in3 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_1fc_in3_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_1layer.json cnn1layer_8_mp Nets/cnn1layer_8_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_8_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_1layer.json cnn1layer_16_mp Nets/cnn1layer_16_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_16_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_1layer.json cnn1layer_32_mp Nets/cnn1layer_32_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_32_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_1layer.json cnn1layer_64_mp Nets/cnn1layer_64_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_64_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_1layer.json cnn1layer_8_mp_1fc Nets/cnn1layer_8_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_1layer.json cnn1layer_16_mp_1fc Nets/cnn1layer_16_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_1layer.json cnn1layer_32_mp_1fc Nets/cnn1layer_32_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_1layer.json cnn1layer_64_mp_1fc Nets/cnn1layer_64_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn1layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn1layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_2layer.json cnn2layer_8_mp Nets/cnn2layer_8_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_8_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_2layer.json cnn2layer_16_mp Nets/cnn2layer_16_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_16_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_2layer.json cnn2layer_32_mp Nets/cnn2layer_32_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_32_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_2layer.json cnn2layer_64_mp Nets/cnn2layer_64_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_64_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_2layer.json cnn2layer_8_mp_1fc Nets/cnn2layer_8_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_2layer.json cnn2layer_16_mp_1fc Nets/cnn2layer_16_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_2layer.json cnn2layer_32_mp_1fc Nets/cnn2layer_32_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_2layer.json cnn2layer_64_mp_1fc Nets/cnn2layer_64_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn2layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn2layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_3layer.json cnn3layer_8_mp Nets/cnn3layer_8_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_8_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_3layer.json cnn3layer_16_mp Nets/cnn3layer_16_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_16_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_3layer.json cnn3layer_32_mp Nets/cnn3layer_32_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_32_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_3layer.json cnn3layer_64_mp Nets/cnn3layer_64_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_64_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_3layer.json cnn3layer_8_mp_1fc Nets/cnn3layer_8_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_3layer.json cnn3layer_16_mp_1fc Nets/cnn3layer_16_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_3layer.json cnn3layer_32_mp_1fc Nets/cnn3layer_32_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_3layer.json cnn3layer_64_mp_1fc Nets/cnn3layer_64_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn3layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn3layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_4layer.json cnn4layer_8_mp Nets/cnn4layer_8_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_8_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_4layer.json cnn4layer_16_mp Nets/cnn4layer_16_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_16_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_4layer.json cnn4layer_32_mp Nets/cnn4layer_32_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_32_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_4layer.json cnn4layer_64_mp Nets/cnn4layer_64_mp_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_64_mp_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_4layer.json cnn4layer_8_mp_1fc Nets/cnn4layer_8_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_8_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_4layer.json cnn4layer_16_mp_1fc Nets/cnn4layer_16_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_16_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_4layer.json cnn4layer_32_mp_1fc Nets/cnn4layer_32_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_32_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue

arguments = Code/testvideo_tail.py Code/input_4.json Code/extrusion_networks_4layer.json cnn4layer_64_mp_1fc Nets/cnn4layer_64_mp_1fc_in4 video68_2017-05-29T10_54_29/video68_2017-05-29T10_54_29.avi video68_2017-05-29T10_54_29/trajectories_nogaps.txt video68_2017-05-29T10_54_29/MA_video68.csv cnn4layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.csv
output = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.out
error = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.err
log = /cluster/vasconcelos/vasconcelos2/tensorflow-dnn/cnn4layer_64_mp_1fc_in4_video68_2017-05-29T10_54_29.log

queue