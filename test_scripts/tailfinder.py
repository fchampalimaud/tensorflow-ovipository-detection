import cv2
import numpy as np
import os

from mcvapi.blobs.biggests_blobs import BiggestsBlobs
from mcvapi.blobs.find_blobs import FindBlobs
from mcvapi.filters.adaptative_threshold import AdaptativeThreshold
from mcvapi.blobs.blob_image import BlobImage
from mcvapi.blobs.extreme_points import BlobExtremePoints

class Pipeline(
    BlobImage,
    BlobExtremePoints,
    BiggestsBlobs,
    FindBlobs,
    AdaptativeThreshold
): 
    def __init__(self):
        super(Pipeline,self).__init__()
        self._param_adaptivethreshold_block_size = 160
        self._param_adaptivethreshold_c = 70
        self._param_adaptivethreshold_method = 0
        self._param_adaptivethreshold_type = 1

def tailfinder_image(im):
    pipeline = Pipeline()

    img = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)

    blobs = pipeline.processflow(img,oimage = img)

    print(blobs)
    margin = 10
    margin2 = 25
    ath = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,161,0)

    #cv2.imshow('bla',ath)

    saveimg = None

    for i, blob in enumerate(blobs):
        #print(blob._head,blob._tail)
        if blob._head[0] > margin and blob._head[1] > margin and blob._tail[0] > margin and blob._tail[1] > margin:
            #cv2.imshow('blob-{0}'.format(i),blob._oimage)
            #blob.draw(im)
            cut1 = ath[blob._head[1]-margin:blob._head[1]+margin,blob._head[0]-margin:blob._head[0]+margin]            
            #cv2.imshow('cut1',cut1)
            cut2 = ath[blob._tail[1]-margin:blob._tail[1]+margin,blob._tail[0]-margin:blob._tail[0]+margin]
            #cv2.imshow('cut2',cut2)
            print('nonzero:',cv2.countNonZero(cut1),cv2.countNonZero(cut2))
            #if cut1.mean() > cut2.mean():
            if cv2.countNonZero(cut1) > cv2.countNonZero(cut2):
                saveimg = im[blob._tail[1]-margin2:blob._tail[1]+margin2,blob._tail[0]-margin2:blob._tail[0]+margin2]
                return saveimg
                #cv2.rectangle(im,(blob._tail[0]-margin2,blob._tail[1]-margin2),(blob._tail[0]+margin2,blob._tail[1]+margin2),(0,0,255),4)
            else:
                saveimg = im[blob._head[1]-margin2:blob._head[1]+margin2,blob._head[0]-margin2:blob._head[0]+margin2]
                return saveimg
                #cv2.rectangle(im,(blob._head[0]-margin2,blob._head[1]-margin2),(blob._head[0]+margin2,blob._head[1]+margin2),(0,0,255),4)
            #cv2.imshow('frame',im)
            #cv2.waitKey(10)
    
    return saveimg

def tailfinder(filename, orig, dest):
    #load our test image:
    totalfilename = orig+filename
    im = cv2.imread(totalfilename)
    img = cv2.imread(totalfilename,cv2.IMREAD_GRAYSCALE)
    pipeline = Pipeline()

    blobs = pipeline.processflow(im,oimage = im)

    print(blobs)
    margin = 10
    margin2 = 25
    ath = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,161,0)

    #cv2.imshow('bla',ath)

    for i, blob in enumerate(blobs):
        print(blob._head,blob._tail)
        if blob._head[0] > margin and blob._head[1] > margin and blob._tail[0] > margin and blob._tail[1] > margin:
            #cv2.imshow('blob-{0}'.format(i),blob._oimage)
            #blob.draw(im)
            cut1 = ath[blob._head[1]-margin:blob._head[1]+margin,blob._head[0]-margin:blob._head[0]+margin]            
            #cv2.imshow('cut1',cut1)
            cut2 = ath[blob._tail[1]-margin:blob._tail[1]+margin,blob._tail[0]-margin:blob._tail[0]+margin]
            #cv2.imshow('cut2',cut2)
            print('nonzero:',cv2.countNonZero(cut1),cv2.countNonZero(cut2))
            #if cut1.mean() > cut2.mean():
            if cv2.countNonZero(cut1) > cv2.countNonZero(cut2):
                saveimg = im[blob._tail[1]-margin2:blob._tail[1]+margin2,blob._tail[0]-margin2:blob._tail[0]+margin2]
                cv2.imwrite(dest+filename,saveimg)
                print('writing image',dest+filename)
                #cv2.rectangle(im,(blob._tail[0]-margin2,blob._tail[1]-margin2),(blob._tail[0]+margin2,blob._tail[1]+margin2),(0,0,255),4)
            else:
                saveimg = im[blob._head[1]-margin2:blob._head[1]+margin2,blob._head[0]-margin2:blob._head[0]+margin2]
                cv2.imwrite(dest+filename,saveimg)
                print('writing image',dest+filename)
                #cv2.rectangle(im,(blob._head[0]-margin2,blob._head[1]-margin2),(blob._head[0]+margin2,blob._head[1]+margin2),(0,0,255),4)
            #cv2.imshow('frame',im)
            #cv2.waitKey(10)

if __name__ == '__main__':
    origfolder = 'C:/CODE/DATASETS/female/'
    dstfolder = 'C:/CODE/DATASETS/female_tail/'
    for filename in os.listdir(origfolder):
        tailfinder(filename,origfolder,dstfolder)
    
    origfolder = 'C:/CODE/DATASETS/extrusion2/'
    dstfolder = 'C:/CODE/DATASETS/extrusion2_tail/'
    for filename in os.listdir(origfolder):
        tailfinder(filename,origfolder,dstfolder)
    
    origfolder = 'C:/CODE/DATASETS/extrusion3/'
    dstfolder = 'C:/CODE/DATASETS/extrusion3_tail/'
    for filename in os.listdir(origfolder):
        tailfinder(filename,origfolder,dstfolder)
