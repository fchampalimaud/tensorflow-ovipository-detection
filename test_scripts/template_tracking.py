import cv2
import csv
import math
import mcvapi
from tensorflow_networks import videoreader

import tailfinder


def readcsv(filename, startingrow):
    t1 = []
    t2 = []
    with open(filename, 'U') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)
        spamreader = csv.reader(csvfile, dialect)
        skiped = False
        for row in spamreader:
            if skiped == False:
                startingrow = startingrow - 1
                if startingrow == 0:
                    skiped = True
            else:
                t1.append((float(row[0]), float(row[1])))
                t2.append((float(row[3]), float(row[4])))

    return t1, t2


reader = videoreader.videoreader()
result = reader.open('G:/My Drive/OvipositorExtrusion/video21_2017-05-18T10_40_09/video21_2017-05-18T10_40_09.avi')

t1, t2 = readcsv('G:/My Drive/OvipositorExtrusion/video21_2017-05-18T10_40_09/trajectories_nogaps.txt', 1)

currentframe = 0



if result:
    im = reader.getframe()
    if im is not None and currentframe < len(t2)-1 or currentframe < len(t2) - 1:
        currentframe = 1
        if len(t2) > 0 and not math.isnan(
            t2[currentframe][0]) and not math.isnan(
                t2[currentframe][1]):
                roi1 = ((int(t2[currentframe][0]) - 80,
                        int(t2[currentframe][1]) - 80),
                        (int(t2[currentframe][0]) + 80,
                        int(t2[currentframe][1]) + 80))
                object2 = im[roi1[0][1]:roi1[1][1], roi1[0][0]:roi1[1][0]]
        cv2.namedWindow('tracking')
        bbox = cv2.selectROI('tracking',object2)
        print(bbox)
        #x1 = bbox[0]
        
        #template = object2[bbox[0]:bbox[2],bbox[1]:bbox[3]]
        #cv2.imshow('template',template)
        #cv2.waitKey(0)
        tracker = cv2.TrackerTLD_create()
        init_once = False
    
    while(1):
        im = reader.getframe()
        if im is not None:
            currentframe = currentframe + 1
            if len(t2) > 0 and not math.isnan(
            t2[currentframe][0]) and not math.isnan(
                t2[currentframe][1]):
                roi1 = ((int(t2[currentframe][0]) - 80,
                        int(t2[currentframe][1]) - 80),
                        (int(t2[currentframe][0]) + 80,
                        int(t2[currentframe][1]) + 80))
                object2 = im[roi1[0][1]:roi1[1][1], roi1[0][0]:roi1[1][0]]
            if not init_once:
                ok = tracker.init(object2, bbox)
                init_once = True
            
            tail = tailfinder.tailfinder_image(object2)
            cv2.imshow('tail',tail)

            ok, newbox = tracker.update(object2)
            print (ok, newbox)
            if ok:
                p1 = (int(newbox[0]), int(newbox[1]))
                p2 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3]))
                cv2.rectangle(object2, p1, p2, (200,0,0))

            cv2.imshow("tracking", object2)
            k = cv2.waitKey(1) & 0xff
            if k == 27 : break # esc presse
'''


def click_and_crop(event, x, y, flags, param):
	# grab references to the global variables
	global refPt, cropping
 
	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt = [(x, y)]
		cropping = True
 
	# check to see if the left mouse button was released
	elif event == cv2.EVENT_LBUTTONUP:
		# record the ending (x, y) coordinates and indicate that
		# the cropping operation is finished
		refPt.append((x, y))
		cropping = False
 
		# draw a rectangle around the region of interest
		cv2.rectangle(im, refPt[0], refPt[1], (0, 255, 0), 2)
		cv2.imshow("image", im)


reader = videoreader.videoreader()
result = reader.open('G:/My Drive/OvipositorExtrusion/video21_2017-05-18T10_40_09/video21_2017-05-18T10_40_09.avi')

cv2.namedWindow("image")
cv2.setMouseCallback("image", click_and_crop)

if result:
    im = reader.getframe()
    if im is not None:
        clone = im.copy()
        cv2.imshow("image",im)
        # keep looping until the 'q' key is pressed
        while True:
            # display the image and wait for a keypress
            cv2.imshow("image", im)
            key = cv2.waitKey(1) & 0xFF
        
            # if the 'r' key is pressed, reset the cropping region
            if key == ord("r"):
                im = clone.copy()
        
            # if the 'c' key is pressed, break from the loop
            elif key == ord("c"):
                break
    
    if len(refPt) == 2:
        roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
        w, h = roi.shape[:2]
        cv2.imshow("ROI", roi)
        cv2.waitKey(0)

    while(1):
        im = reader.getframe()
        if im is not None:
            res = cv2.matchTemplate(im,roi,cv2.TM_CCOEFF_NORMED)
            cv2.imshow('match',res)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
            top_left = max_loc
            bottom_right = (top_left[0] + w, top_left[1] + h)
            cv2.rectangle(im,top_left, bottom_right, (0,0,255), 2)
            cv2.imshow('image',im)
            cv2.waitKey(1)
'''