import sys
import cv2
import csv
import copy
import math
from tensorflow_networks import videoreader
from tensorflow_networks.native import tensorflow_net_builder
from tensorflow_networks import tools


def read_events(file):
    print('Reading csv file', file)
    ext_list = []
    with open(file, 'U') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        numfiles = 0
        extrusion_events = False
        for row in spamreader:
            if len(row) > 0:
                if row[0] == 'T' and row[1] == 'OE':
                    extrusion_events = True
                if row[0] == 'T' and row[1] != 'OE':
                    extrusion_events = False
                if row[0] == 'P' and extrusion_events is True:
                    i = int(row[2])
                    j = int(row[3])
                    ext_list.append((i, j))
    return ext_list


def isextrusionevent(framenumber, extrusion_list):
    isevent = 0
    for event in extrusion_list:
        if event[0] <= framenumber <= event[1]:
            isevent = 1
    return isevent


def readcsv(filename, startingrow):
    t1 = []
    t2 = []
    with open(filename, 'U') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)
        spamreader = csv.reader(csvfile, dialect)
        skiped = False
        for row in spamreader:
            if skiped == False:
                startingrow = startingrow - 1
                if startingrow == 0:
                    skiped = True
            else:
                t1.append((float(row[0]), float(row[1])))
                t2.append((float(row[3]), float(row[4])))

    return t1, t2

def main(argv):

    print('init')

    input_file = argv[0]
    network_file = argv[1]
    network_name = argv[2]
    network_weights = argv[3]
    video_name = argv[4]
    trajectories_file = argv[5]
    event_file = argv[6]
    output_file = argv[7]


    result_list = []

    print('loading video')

    video = videoreader.videoreader()

    print(video)

    design, image_size, imgclasses, num_samples, data, epochs = tools.netparser_2(input_file,
                                                                                  network_file, 
                                                                                  network_name)

    print('laoding network')

    cnn = tensorflow_net_builder.netbuilder()
    cnn.GenerateInput(image_size, 3, ["extrusion", "noextrusion"])
    cnn.GenerateModel(design)
    cnn.Load(network_weights + '.tf')

    event_list = read_events(event_file)

    tp = 0
    tn = 0
    fp = 0
    fn = 0

    print('csvreading')

    # we also need to get the detected paths using the csv generated from idtracker
    t1, t2 = readcsv(trajectories_file, 1)

    video.release()

    video_ok = video.open(video_name)

    print('video ok:', video_ok)

    if video_ok:
        print('VIDEO OPEN... START ANALYSING')
        currentframe = 0
        while (1):
                videoframe = None
                videoframe = video.getframe()
                if videoframe is None or currentframe > len(t1)-1 or currentframe > len(t2) - 1:
                    acc = 0
                    precision = 0
                    recall = 0
                    tnrate = 0
                    if tp > 0 or tn > 0 or fp > 0 or fn > 0:
                        acc = (tp + tn) / (tp + tn + fp + fn)
                    if tp > 0 or fp > 0:
                        precision = tp / (tp + fp)
                    if tp > 0 or fn > 0:
                        recall = tp / (tp + fn)
                    if tn > 0 or fp > 0:
                        tnrate = tn / (tn + fp)
                    print('==========')
                    print('VIDEO:',video_name)
                    print('true positives:', tp)
                    print('true negatives:', tn)
                    print('false positives', fp)
                    print('false negatives:', fn)
                    print('accuracy:',round(acc, 2))
                    print('precision:',round(precision, 2))
                    print('recall:',round(recall, 2))
                    print('true negative rate:', round(tnrate, 2))
                    print('==========')

                    # we shall wite the csv file here
                    with open(output_file,'w') as csvfile:
                        fieldnames = ['Frame','Result','R1_0','R1_1','R2_0','R2_1']
                        writer = csv.DictWriter(csvfile,fieldnames = fieldnames)

                        writer.writeheader()
                        for item in result_list:
                            writer.writerow({'Frame': item[0],'Result': item[1],'R1_0': item[2],'R1_1': item[3],'R2_0': item[4],'R2_1': item[5]})

                    break

                aux = copy.copy(videoframe)

                isevent = 0
                eventfound = False
                extrusiondetected = False
                if isextrusionevent(currentframe, event_list):
                    isevent = 255
                    eventfound = True

                if len(t1) > 0 and not math.isnan(
                        t1[currentframe][0]) and not math.isnan(
                            t1[currentframe][1]):
                    roi1 = ((int(t1[currentframe][0]) - 80,
                            int(t1[currentframe][1]) - 80),
                            (int(t1[currentframe][0]) + 80,
                            int(t1[currentframe][1]) + 80))
                    object1 = aux[roi1[0][1]:roi1[1][1], roi1[0][0]:roi1[1][0]]
                    result1 = cnn.predict(object1)
                    #print(result1)
                    if result1[0][0] > result1[0][1]:
                        extrusiondetected = True


                if len(t2) > 0 and not math.isnan(
                        t2[currentframe][0]) and not math.isnan(
                            t2[currentframe][1]):
                    roi2 = ((int(t2[currentframe][0]) - 80,
                            int(t2[currentframe][1]) - 80),
                            (int(t2[currentframe][0]) + 80,
                            int(t2[currentframe][1]) + 80))
                    object2 = aux[roi2[0][1]:roi2[1][1], roi2[0][0]:roi2[1][0]]
                    result2 = cnn.predict(object2)
                    if result2[0][0] > result2[0][1]:
                        extrusiondetected = True

                        

                if eventfound and extrusiondetected:
                    tp = tp + 1
                elif eventfound and not extrusiondetected:
                    fn = fn + 1
                elif not eventfound and extrusiondetected:
                    fp = fp + 1
                elif not eventfound and not extrusiondetected:
                    tn = tn + 1

                if extrusiondetected:
                    result_list.append((currentframe,1,result1[0][0],result1[0][1],result2[0][0],result2[0][1]))
                else:
                    result_list.append((currentframe,0,result1[0][0],result1[0][1],result2[0][0],result2[0][1]))

                #print('frame:',currentframe)

                currentframe = currentframe + 1
    
    print('Exiting')

if __name__ == '__main__':
    '''
    This shall receive as arguments the input json, the network json and the network name
    '''
    main(sys.argv[1:])