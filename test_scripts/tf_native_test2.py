import sys
#import tensorflow_networks.native.tensorflow_net_builder as networks
import tensorflow_networks.tools as tools

#import tensorflow_networks.dataset as dataset


def main(argv):

    filename = argv[0]
    layername = argv[1]

    #D:\CODE\Videos\video36_2017-05-26T10_19_49\Images\contours-images\video36_2017-05-26T10_19_49
    print('starting neural network')

    design, image_size, imgclasses, num_samples, data, epochs = tools.netparser(
        filename, layername)

    model = networks.netbuilder()
    model.GetImages(data, 160, 3, imgclasses, 0.2, num_samples)
    print('images read')
    model.GenerateModel(design)
    model.Train(n_epochs=epochs, _batch_size=16, output_interval=20)
    model.Save(layername + ".tf")


if __name__ == '__main__':
    main(sys.argv[1:])